import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';

import {
  ApexNonAxisChartSeries,
  ApexResponsive,
  ApexChart
} from "ng-apexcharts";

export type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
};

@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.css']
})
export class ReportesComponent implements OnInit {
  chartOptions: Partial<ChartOptions> | any;
  chartOptions5: Partial<ChartOptions> | any;
  reporte1:any = [];
  reporte2:any = [];
  reporte3:any = [];
  reporte4:any = [];
  departamentos:any = [];
  municipios:any = [];
  showReporte2 = false;
  showReporte4 = false;
  showReporte5 = false;

  constructor(private userS: UserService) { }

  ngOnInit(): void {
    this.userS.getListaElecciones().subscribe((result) =>{
      this.reporte1 = result.response;
      this.reporte1.forEach((element: { fecha: string; fechaInicio: any; fechaFin: any; }) => {
        const myArray = element.fecha.split(";");
        element.fechaInicio = myArray[0];
        element.fechaFin = myArray[1];
      });
    });
    this.userS.getDepartamentos().subscribe((result) =>{
      this.departamentos = result;
    });
  }

  Reporte2(){
    const e = document.getElementById("elecciones") as HTMLSelectElement;
    const textoAmostrar = document.getElementById("myTextarea") as HTMLSpanElement;
    const elecciones = e.options[e.selectedIndex].value;
    this.userS.getDashboard(elecciones).subscribe((result) =>{
      this.reporte2 = result;
      textoAmostrar.textContent = "Votantes registrados: "+ result.votantes_registrados+" - "+
      " Votantes votaron: "+ result.votantes_votaron;
      const Series: any[] = [];
      const Labels: any[] = [];
      this.reporte2.resultados_opcion.forEach((element: { total: any; titulo: any; }) => {
        Series.push(element.total);
        Labels.push(element.titulo);
      });
      this.chartOptions = {
        series: Series,
        chart: {
          width: 480,
          type: "pie"
        },
        labels: Labels,
        responsive: [
          {
            breakpoint: 480,
            options: {
              chart: {
                width: 200
              },
              legend: {
                position: "bottom"
              }
            }
          }
        ]
      };
      this.showReporte2 = true;
    });
  }

  Reporte4(){
    this.showReporte4 = false;
    this.municipios = []
    const e = document.getElementById("departamento") as HTMLSelectElement;
    const elecciones = e.options[e.selectedIndex].value;
    this.userS.getResultadosGraficaMunicipio(elecciones).subscribe((result) =>{
      this.reporte4 = result.response[0].resultados;
      console.log(this.reporte4);
      this.reporte4.forEach((element: { id: any; nombre: any; }) => {
        this.municipios.push({id:element.id, nombre: element.nombre});
      });
      console.log(this.municipios);
      this.showReporte4 = true;
    });
  }

  Reporte5(){
    const e = document.getElementById("municipio") as HTMLSelectElement;
    const elecciones = e.options[e.selectedIndex].value;
    const Series: any[] = [];
    const Labels: any[] = [];
    this.reporte4.forEach((elements: { id: string; opciones: any[]; }) => {
      if(elements.id == elecciones){
        console.log(elements);
        elements.opciones.forEach(element => {
          const total = element.votos_fisicos ? element.votos_fisicos : element.votos_electronicos;
          Series.push(total);
          Labels.push(element.titulo);
        });
      }
    });
    this.chartOptions5 = {
      series: [{
        name: "total",
        data: Series
      }],
      chart: {
        type: "bar",
        height: 350
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: "55%",
          endingShape: "rounded"
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        show: true,
        width: 2,
        colors: ["transparent"]
      },
      xaxis: {
        categories: Labels
      },
      yaxis: {
        title: {
          text: "$ (thousands)"
        }
      },
      fill: {
        opacity: 1
      }
    };
    this.showReporte5 = true;
  }

}
