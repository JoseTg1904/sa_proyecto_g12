import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  paises:any = [];
  municipios:any = [];
  fotografia_personal: any = "";
  fotografia_dpi: any = "";
  email = "";
  password = "";
  confirmPassword = "";
  phoneNumber = "";
  cui = "";

  constructor(private toastr: ToastrService,private userS: UserService,private router: Router) { }

  ngOnInit(){
    this.userS.getPaises().subscribe((result) =>{
        this.paises = result;
    });

    this.userS.getMunicipios().subscribe((result) =>{
      this.municipios = result;
    });
  }

  register(){
    const e = document.getElementById("pais") as HTMLSelectElement;
    const pais = e.options[e.selectedIndex].value;
    const c = document.getElementById("municipio") as HTMLSelectElement;
    const municipio = c.options[c.selectedIndex].value;

    if(this.password == "" || this.cui == "" || this.email == "" || this.fotografia_dpi == "" || this.fotografia_personal == "" || this.phoneNumber == ""){
      this.toastr.warning('Llenar todos los campos');
      return
    }

    if(this.password !== this.confirmPassword){
      this.toastr.warning('Las contrasenias no son iguales');
      return
    }
    this.userS.register(
      this.email,this.password,
      this.phoneNumber,this.cui,pais,municipio,this.fotografia_dpi,this.fotografia_personal
    ).subscribe(
      (result) =>{
        console.log(result);
        this.toastr.success('Usuario registrado');
        this.router.navigate(['/']);},
      (err)=>{this.toastr.error(err.error.msg);
      });
  }

  preview(files: any){
    if (files.length === 0){
      this.toastr.error('No Hay Imagen Seleccionada', 'Error!');
      return;
    }
    this.toastr.info('Imagen Cargada', 'Aviso!');
    const reader = new FileReader();
    reader.readAsDataURL(files[0]);
    reader.onload = ( _event ) => {
      this.fotografia_personal = reader.result;
    };
  }

  previewDPI(files: any){
    if (files.length === 0){
      this.toastr.error('No Hay Imagen Seleccionada', 'Error!');
      return;
    }
    this.toastr.info('Imagen Cargada', 'Aviso!');
    const reader = new FileReader();
    reader.readAsDataURL(files[0]);
    reader.onload = ( _event ) => {
      this.fotografia_dpi = reader.result;
    };
  }
}
