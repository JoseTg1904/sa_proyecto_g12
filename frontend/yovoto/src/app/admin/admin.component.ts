import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  elecciones:any = [];
  validar = "";
  informacion = "";
  tituloEleccion = "";
  descripcionEleccion = "";
  fechaInicio = "";
  fechaFin = "";
  tituloOpcion = "";

  constructor(private toastr: ToastrService,private userS: UserService) { }

  ngOnInit(): void {
    this.getElecciones();
  }

  getElecciones(){
    this.userS.getListaElecciones().subscribe((result) =>{
      this.elecciones = result.response;
    });
  }

  validarRenap(){
    const token = this.userS.getLogin().token;
    this.userS.getValidarRenap(this.validar,token).subscribe((result) =>{
      this.toastr.success('El usuario existe');
      console.log(result);
  },
  (err: HttpErrorResponse) => {
    this.toastr.warning('El usuario no existe');
    console.log(err);
  });
  }

  informacionRenap(){
    const token = this.userS.getLogin().token;
    const textoAmostrar = document.getElementById("myTextarea") as HTMLSpanElement;
    this.userS.getInformacionRenap(this.informacion,token).subscribe((result) =>{
      this.toastr.success('El usuario existe');
      textoAmostrar.textContent = "CUI: "+result.cui+" -- Nombres: "+result.nombres+" -- Apellidos: "+result.apellidos+" -- Fecha de nacimiento: "+result.fecha_nacimiento+" -- Puede votar: "+ String(result.puede_votar == "0"? "No":"Si");
    },
    (err: HttpErrorResponse) => {
      this.toastr.warning('El usuario no existe');
      textoAmostrar.textContent = "";
      console.log(err);
    });
  }

  preview(files: any){
    const token = this.userS.getLogin().token;
    if (files.length === 0){
      this.toastr.error('No Hay Json Seleccionado', 'Error!');
      return;
    }
    let resultJSON = "";
    this.toastr.info('JSON cargado correctamente', 'Aviso!');
    const reader = new FileReader();
    reader.readAsText(files[0]);
    reader.onload = function() {
      resultJSON = JSON.parse(String(reader.result));
    };
    setTimeout(() => {
      this.userS.crearRenap(resultJSON,token).subscribe((result) =>{
        this.toastr.success('Usuarios insertados a renap correctamente');
        console.log(result);
      },
      (err: HttpErrorResponse) => {
        this.toastr.error('Error al insertar el archivo');
        console.log(err);
      });
    }, 2000);
  }

  preview2(files: any){
    const token = this.userS.getLogin().token;
    if (files.length === 0){
      this.toastr.error('No Hay Json Seleccionado', 'Error!');
      return;
    }
    let resultJSON = "";
    this.toastr.info('JSON cargado correctamente', 'Aviso!');
    const reader = new FileReader();
    reader.readAsText(files[0]);
    reader.onload = function() {
      resultJSON = JSON.parse(String(reader.result));
    };
    setTimeout(() => {
      this.userS.votosFisicos(resultJSON,token).subscribe((result) =>{
        this.toastr.success('Votos registrados correctamente');
        console.log(result);
      },
      (err: HttpErrorResponse) => {
        this.toastr.error('Error al insertar el archivo');
        console.log(err);
      });
    }, 2000);
  }

  crearEleccion(){
    const token = this.userS.getLogin().token;
    const d = new Date(this.fechaInicio);
    const f = new Date(this.fechaFin);
    const datestring1 = d.getDate()  + "/" + (d.getMonth()+1) + "/" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes();
    const datestring2 = f.getDate()  + "/" + (f.getMonth()+1) + "/" + f.getFullYear() + " " + f.getHours() + ":" + f.getMinutes();
    const resultJSON = {
      "titulo": this.tituloEleccion, 
      "descripcion": this.descripcionEleccion,
      "fecha_inicio": datestring1,
      "fecha_fin": datestring2
    };
    this.userS.crearEleccion(resultJSON,token).subscribe((result) =>{
      this.toastr.success('Eleccion creada correctamente');
      console.log(result);
      this.tituloEleccion = "";
      this.descripcionEleccion = "";
      this.fechaInicio = "";
      this.fechaFin = "";
      this.getElecciones();
    },
    (err: HttpErrorResponse) => {
      this.toastr.error('Error al crear eleccion');
      console.log(err);
    });
  }

  crearOpcion(){
    const token = this.userS.getLogin().token;
    const e = document.getElementById("elecciones") as HTMLSelectElement;
    const elecciones = e.options[e.selectedIndex].value;
    const resultJSON = {
      "titulo": this.tituloOpcion,
      "extension": "jpeg",
      "eleccion": elecciones,
      "metadatos": "partido=test",
      "imagen": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAH8AugMBEQACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAAAwUEBgcCAQj/xAA3EAABAwIEBAMGBAYDAAAAAAABAAIDBBEFEiExBkFRYRMigQcycaGx0SMkkfBCUnLBwuEUF1P/xAAbAQEAAgMBAQAAAAAAAAAAAAAAAwQBAgUGB//EADERAQACAgEDAgMGBQUAAAAAAAABAgMRBBIhMQVBEyJRMmFxkaHRBhRCwfAjMzRSgf/aAAwDAQACEQMRAD8A7igICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg8SSMiY58jg1rRcucbABYmdD01zXNDmkEEXBHNZ8j6gICAgICAgICAgICAgICAgIMOtxKloR+Yla13JvMqPJlpjjvKbDx8mafkhS1PGNJAMxZ5eXm3VSebO+1XQx+kZb9olUVHtKpYn5RSZhzPiWW8cm//AFXa/wAPXmO9/wBE1L7ScNlt48Ekd+jwfst45H1hHk/h/PX7NoldwcV4PUMu2qa11vdcCCVv8emnPv6byqT3qxXUs/E8XiVM8lNh7h5II7Z3d3Hl1so4rbN3t4RTMcedRHzNgpKeOlpooIRaONga0E30HdWYiKxqFaZmZ3KZZYEBAQEBAQEBAQEGBjcVZLh8rMOlMVQbZXC3XXfso8nV0z0+W1Onq+bwyKWcTxB+Utdezmndp6Jiv112Xr0zpOpGogIKfiLGG4VSktIMz9GA7DuVFlydEfeu8HhzycmvaPLl2JYxJJI9z5HPc7e51uuf9qdvZYOHWtYiI0qXxVde1vnLWHUWct4iIW4tTGjfgz2Al783xCzuG9eRX2hNT8PeK4mSR1htZbbR5OXEeITyYDLGPytRIHjU5joiD+ZrbtaE1HiGJ4U7Oyd7Ducrv3dI3E/Kr5uLhzRqa7bnwxx7HWSMpcSs2R1g2Vu1z1CnpmnerOFzvR5xRNsX5N8abi4N1YcJ9QEBAQEBAQEBAQEFfBePGKmMnySxskaO4uHf4qtT5c9o+sRP9v2TW74qz9Nx/eFgrKEQeXaC+yDkPGmMmpq5ZQ/yZsrOw7D5rnXt12mXufSeH8PHEa7tapQaiWMvccrrE6c9vuseHXt8lZ1DaIIGMiFwBbss+zkWvMyhrZGxtyXubXt0CwkxVm3zJcMkY4Fml9xfosw05FZjus2x3HK1lsp9SpxiBrYydNjuseFzBeZaXNOI5j4Rc0jR2un6Lb8XRnHMx3dm9muPnGMF8KZ156Y5CTuW8irGG241LxPq3E/l8+48S29TOUICAgICAgICAgIK+sPh4lQPt7z3xk/Ft/q0KpliYz47fjH6b/smx98d4/Cf8/NYK2hEFbxDUGlwipkafNlyj1NlHlt00mVrhY4yZ61lwnG356iS4uWuyjt6LnRL6Nxq/JGnvCHeFUBs2rmafA9Fv7HIrum4XktYGw6HbfVNqNcO7KOSqdNKZXl5Oxcttdl+Mdax0wyKWqDHaOG+gBWs9mmTF1QuosUdkJJzHrZY6nOtxo2w8TrrxFxNtL6rO9p+Ph+bs1KUtc6chzSWkkEH3tbafVZ6tr0zrTdvZLWOi4idT580U0Lmg2ttqPop8M6s876/ji2CL/SXZhsrTx4gICAgICAgICAgrMaLWNpXvHu1Udj0JNv7qtyZ1FbffH7JsEbm0fdLPc/TRWUL7nA9EFJxc8HBXD+aRoVfk/7boemf8iP/AFxetpXzVEjdBmfmaXc/X0VF9BxZa1pEpaaB8DBG1tybHrd1lmJa2yReer/NIpp5T5XFgDL2BFw7sVvptWtfMe6LxHRxGMsBJ8tr6aaLfZNItbr2hD3NJdZmrRqBsOy1lNqJ7M0NGuQuc7XJYHUdVr48oer6q2Z8r3Fr3uOmmZ3IBNytxFa17Qw7uNwMzY7jMta1narbW25+ymAu4micBYR5vXyq1i+1Dz/rltcaYdvGytvGiAgICAgICAg+HZBgYjikFCLPOaQi4Y1RZc1cflPg4183jw1vFOJf+RGIDHAyz2vB8XMbtcD07Lncnk/Er01j3j3+kxLq4fTLVnq3P5fVLBxrQ7VYZG6+0Umf6gK7Xk190d/Rs0fY7/o2KgrKavpvFpJRJGTuOSnraLRuHLy4r4rdN41LD4np3TYJUAaloDrdgdfldRciN45WOBfo5FZcgmka6cRkeUO6rldeu73lKarvfdLFHG+M2cWhoBd36LbqaW6onSKKnY5shytL73DXDQhZm2pb3tMa79kLqMZWuDWuEh94m1j+ysxkSfE7z38InUgLQGt1AyuObe62jI3jJO9yncwMjMbmWkH8Q+a169o43NtxPZh1kMZDsgaLN87gNOWo7resp6XtrvLBFIXWjLgHkAm5330Hy3U0TruivkdJ9lmH2qpqkDyRx5W/E/sqXBEzbbyvreaJiKfWXTVaecEBAQEBAQEBBFVSiCnlldsxpcfRYtPTG21Kze0Vj3cVqsYqsVx2UzOIgzkmy5Ft5J6pfQMPEx8fjxFY7vFZiVHS1QiDbNDCXc79AtZrEQ3x4Ml6dSAR0FXRXYWCXJffUOU2Lw2mctL/AHLD2X43LQ8SDDJSTT1N2gfyv3H91PhmYyacv17i1yYPi18w7NMxssbo3C4cCCrkxuNPG1tNZiY9nF66kdS4jNTPF3CTL8ehXByV6bafQcOaMmGt4+gYw6I2blNxe3JZrJ1at5fJGeHZge1rhoSDvdZ3tmJ330hLA2NlyMt+TtSsblJFt2lI2mz5Q1rWk225p1NZy9PmXqqp2xt8OMh2bTUagrEWa48k2+afZX1sbbPhNmuG7b9O6mpKetp11ezFpohJJ+Fcg3AJ3Vj2V8uTTtPB2GOwzBYmSC00v4kg6XGg/RXcVemrxPOz/GzTMeI7L1SKYgICAgICAgIMbEmGTD6lgFyY3WHoo8sbpMJcFunLWfvcGrovBfMGOyyNc62U2581z6+IfRcV+rW4UT3PdK50jiXHmeaxNdyu+I7PDXOjLSzfot6xpHM7XnA7XT8aYWCCSJg+47Lem/iQ5fq1tcS/4P0Kr7wDROO8LEdZHXRM0l8ryBs4fdcznY+mev6vSejcndJxT7eGvPj8oLW2e4WuNVz6y6sW792OfCcwuc1oB0N9CD1W3dL80TqGMWMdJlAAt81smi1ojbNgYAWtZG4OtqCbeq0mVe9veZRVThYgAAkbk31+K2rG22OFPO43Ia3z3vpf97KzSq1adR5bhwFw2auobX1bPy8ZBa0iwkcNtOgVzFTqnc+Hm/Veb07x18y6aBZW3m31AQEBAQEBAQEA7IOW8c8KyQzzVlM0ugkuTb+A9FRy4umdx4eu9J9SresY7z3j9XN5GPEzy8WJ27rWe8PQ2vGmPY+IbDUbLaO0NdupeyvhSennOOYgwscW5aeNw1sd3fZTYqf1S8t656hW/wDoY5/H9nUFYebY9dSRVtNJTztvG8WPZaXpF69Mt8WS2K8Xr5homLYQ/DTklu6EmzJDt/pcPNgvit9z0vF5cciNx5+imqKeI3F7NJ2JUUTLo0yWVsjQHXadABZTQt1ncd0staI4wGi0lst+SRTujrh3bv4Yfil3l1vztr6qatE09MNo4Y4NmrpG1WJNdDBu1h95/wBlbxYZnvLh+oeq0xxNMXe36R+7pMMLII2RxNDGMFmtGwCuRER4eWtabTM28pFlgQEBAQEBAQEBAQeZI2SMLJGhzTuCLgpMb8sxMxO4Us/B/DtQ8vmwekc525yLT4VPotRzuVEajJP5vkHBvDlPI2SHB6Rr2m4OS9k+HSPZiedyZjU5JXbWNaA1osALADkt1V6QEEVRTw1MRiqI2yRndrhcLFqxaNS2pe1J3WdSoqngzBpySIp4if8Ayne0fpeygnjYp9l2nqfJr/UwZvZ/h8m1dXtH9THfVqx/K4/ZPX1rk1+iH/rfDHEeNXYjJbb8VrfoAto49WJ9Z5M+JZ2FcB4FhlS2pginfK03DpZ3O1/VbRirCvk9Qz5I1aWzhoClUn1AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBB/9k="
    };
    this.userS.crearOpcion(resultJSON,token).subscribe((result) =>{
      this.toastr.success('Opcion creada correctamente');
      console.log(result);
      this.tituloOpcion = "";
    },
    (err: HttpErrorResponse) => {
      this.toastr.error('Error al crear opcion');
      console.log(err);
    });
  }
}
