import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  elecciones:any = [];
  opciones:any = [];
  showReporte4 = false;

  constructor(private userS: UserService,private toastr: ToastrService) { }

  ngOnInit(): void {
    this.userS.getListaElecciones().subscribe((result) =>{
      this.elecciones = result.response;
    });
  }

  getOpciones(){
    const token = this.userS.getLogin().token;
    this.showReporte4 = false;
    const e = document.getElementById("elecciones") as HTMLSelectElement;
    const elecciones = e.options[e.selectedIndex].value;
    this.userS.getOpcionEleccion(elecciones,token).subscribe((result) =>{
      this.opciones = result;
      console.log(this.opciones);
      this.showReporte4 = true;
    });
  }

  votar(){
    const e = document.getElementById("opciones") as HTMLSelectElement;
    const opciones = e.options[e.selectedIndex].value;
    const idUsuario = this.userS.getLogin().idUsuario;
    const token = this.userS.getLogin().token;
    const c = document.getElementById("elecciones") as HTMLSelectElement;
    const elecciones = c.options[c.selectedIndex].value;
    const body = {
      "codUsuario": String(idUsuario),
      "codOpcion": opciones,
      "codEleccion": elecciones
  };
    setTimeout(() => {
      this.userS.votarOpcion(body,token).subscribe((result) =>{
        console.log(result);
        this.showReporte4 = false;
        this.toastr.success('Voto realizado correctamente');
      },
      (err: HttpErrorResponse) => {
        if(String(err.error.mensaje).substr(-1)=="0"){
          this.toastr.success('Voto realizado correctamente');
        }else{
          this.toastr.error('No puede votar, probablemente el usuario ya voto','Error!');
        }
      });
    }, 2000);
  }

}
