import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  password: string = "";
  email: string = "";
  cui: string = "";
  constructor(private toastr: ToastrService, private userS: UserService, private router: Router) { }

  ngOnInit(): void {

  }
  loginIn() {
    console.log(this.password, this.email, this.cui);
    if(this.password == "" || this.email == ""){
      this.toastr.warning('Formulario Incompleto');
      return
    }
    if (this.email == "grupo12" && this.password == "gR4p0_12") {
      this.toastr.success('Bienvenido');

      let userA = {idUsuario:0,correo:"grupo12@gmail.com",token:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjAiLCJjdWkiOiJncnVwbzEyIiwicGluIjoiZ1I0cDBfMTIifQ.E1yFquiCOLFHJIZ2Zh1w6T-D32vqVaWzDapxSlTlgoY"}
      this.userS.saveLogin(userA);
      this.router.navigate(['/admin']);
      return
    }
    this.userS.login(this.cui, this.email, this.password).subscribe(
      (result: any) => {
        console.log("result->", result);
        // idUsuario: any; correo?: any; token?: any; cui?: any; pin?: any
        let idUsuario = result.user.ciudadano;
        let email = result.user.email;
        let token = result.token;
        let cui = result.user.cui;
        let pin = result.user.pin;
        let realUser = {idUsuario, email, token, cui, pin};
        if (realUser == undefined) {
          this.toastr.error('El Usuario No Existe En La Base De Datos');
          return
        }
        this.userS.saveLogin(realUser);
        this.toastr.success('Bienvenido: ' + result.user.cui);
        this.router.navigate(['/home']);
      },
      (error: any) => {
        console.log("error->", error);
        this.toastr.warning('Credenciales Incorrectas');
      });
  }
}
