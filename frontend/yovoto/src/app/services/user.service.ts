import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  isLogged:boolean = false;
  isLoggedCambio: Subject<boolean> = new Subject<boolean>();

  server: string = "http://34.125.179.111:5006";
  
  constructor(private http: HttpClient) {
    this.isLoggedCambio.subscribe((value) => {
      this.isLogged = value
    });
  }

  comprobarLogin(){
    if(localStorage.getItem('user')=="")
      this.isLoggedCambio.next(true);
    else
      this.isLoggedCambio.next(false);

      return this.isLogged
  }

  saveLogin(user: { idUsuario: any; correo?: any; token?: any; cui?: any; pin?: any }){
    localStorage.setItem('user', user.idUsuario);
    localStorage.setItem('email', user.correo);
    localStorage.setItem('userJson', JSON.stringify(user));
    this.isLoggedCambio.next(false);
  }
  getLogin(){
    return  JSON.parse(localStorage.getItem('userJson') || '{idUsuario:-1}');
  }
  deleteLogin(){
    localStorage.setItem('user', "");
    localStorage.setItem('email', "");
    localStorage.setItem('userJson', "{}");
    this.isLoggedCambio.next(true);
  }
  login(cui: string, email: string, pin: string){
    const path = `${this.server}/login`;
    return this.http.post<any>(path,{cui:cui, email:email, pin:pin});
  }

  register(email: string,password: string,
    phoneNumber: string,cui: string,pais: string,municipio: string, fotografia_dpi: string, fotografia_personal: string){
    const path = `${this.server}/registro`;
    const extranjero = pais == "1" ? 0 : 1;
    const randomMovil = Math.floor(Math.random() * 3);
    const movil = randomMovil == 0 ? "iPhone" : randomMovil == 1 ? "Android" : "Chrome";
    const ip1 = Math.floor(Math.random() * 255);
    const ip2 = Math.floor(Math.random() * 255);
    const loc1 = Math.floor(Math.random() * 255);
    const loc2 = Math.floor(Math.random() * 255);
    return this.http.post<any>(path,{
      "cui": cui,
      "token": loc1+"ZIR38XQD6ME42"+loc2,
      "extranjero": extranjero,
      "dispositivo_movil": movil,
      "fotografia_personal": fotografia_personal,
      "fotografia_dpi": fotografia_dpi,
      "pin": password,
      "ip": "197.176."+ip1+"."+ip2,
      "ubicacion": "-"+loc1+".249742336, -"+loc2+".02486272",
      "telefono": phoneNumber,
      "correo": email,
      "pais": pais,
      "municipio": municipio
    });
  }

  getPaises(){
    const path = `${this.server}/paises`;
    return this.http.get<any>(path);
  }

  getMunicipios(){
    const path = `${this.server}/municipios`;
    return this.http.get<any>(path);
  }

  getDepartamentos(){
    const path = `${this.server}/departamentos`;
    return this.http.get<any>(path);
  }

  getListaElecciones(){
    const path = `${this.server}/resultados/elecciones`;
    return this.http.get<any>(path);
  }

  getDashboard(id: string){
    const path = `${this.server}/resultados/dashboard/`+id;
    return this.http.get<any>(path);
  }

  getResultadosGraficaDepartamento(){
    const path = `${this.server}/resultados/mapa`;
    return this.http.get<any>(path);
  }

  getResultadosGraficaMunicipio(id: string){
    const path = `${this.server}/resultados/mapa/`+id;
    return this.http.get<any>(path);
  }

  getValidarRenap(cui: string,token: any){
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });
    const requestOptions = { headers: headers };
    const path = `${this.server}/renap/validar/`+cui;
    return this.http.get<any>(path,requestOptions);
  }

  getInformacionRenap(cui: string,token: any){
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });
    const requestOptions = { headers: headers };
    const path = `${this.server}/renap/informacion/`+cui;
    return this.http.get<any>(path,requestOptions);
  }

  crearRenap(body: any,token: any){
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });
    const requestOptions = { headers: headers };
    const path = `${this.server}/renap/crear`;
    return this.http.post<any>(path,body,requestOptions);
  }

  getOpcionEleccion(id: string,token: any){
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });
    const requestOptions = { headers: headers };
    const path = `${this.server}/opcion/obtener/`+id;
    return this.http.get<any>(path,requestOptions);
  }

  votarOpcion(body: any,token: any){
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });
    const requestOptions = { headers: headers };
    const path = `${this.server}/votacion/votar`;
    return this.http.post<any>(path,body,requestOptions);
  }

  votosFisicos(body: any,token: any){
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });
    const requestOptions = { headers: headers };
    const path = `${this.server}/votacion/cargarVotos`;
    return this.http.post<any>(path,body,requestOptions);
  }

  crearEleccion(body: any,token: any){
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });
    const requestOptions = { headers: headers };
    const path = `${this.server}/eleccion/crear`;
    return this.http.post<any>(path,body,requestOptions);
  }

  crearOpcion(body: any,token: any){
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });
    const requestOptions = { headers: headers };
    const path = `${this.server}/opcion/crear`;
    return this.http.post<any>(path,body,requestOptions);
  }
}
