import os
import requests
import json

from flask import Flask, Response, request
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

"""
    Conjunto de variables globales
"""
url_renap    = os.environ['url_renap'] if 'url_renap' in os.environ else "http://localhost:5000"
url_eleccion = os.environ['url_eleccion'] if 'url_eleccion' in os.environ else "http://localhost:5001"
url_login    = os.environ['url_login'] if 'url_login' in os.environ else "http://localhost:5002"
url_votacion = os.environ['url_votacion'] if 'url_votacion' in os.environ else "http://localhost:5003"
url_reportes = os.environ['url_reportes'] if 'url_reportes' in os.environ else "http://localhost:5004"
url_registro = os.environ['url_registro'] if 'url_registro' in os.environ else "http://localhost:5005"

error_json = {'mensaje': 'Algo salio mal {}'}

"""
    Metodos y funciones
"""
@app.route("/renap/crear", methods=["POST"])
def renap_crear():
    try:
        peticion = requests.post(
            url=url_renap + '/renap/crear',
            headers=request.headers,
            json=request.json
        )

        return handle_response(peticion.json(), peticion.status_code)
    except Exception as err:
        return handle_error(err)

@app.route("/renap/validar/<cui>", methods=["GET"])
def renap_validar(cui):
    try:
        peticion = requests.get(
            url_renap + '/renap/validar/{}'.format(cui),
            headers=request.headers
        )

        return handle_response(peticion.json(), peticion.status_code)
    except Exception as err:
        return handle_error(err)

@app.route("/renap/informacion/<cui>", methods=["GET"])
def renap_informacion(cui):
    try:
        peticion = requests.get(
            url_renap + '/renap/informacion/{}'.format(cui),
            headers=request.headers
        )

        return handle_response(peticion.json(), peticion.status_code)
    except Exception as err:
        return handle_error(err)

@app.route("/eleccion/crear", methods=["POST"])
def eleccion_crear():
    try:
        peticion = requests.post(
            url_eleccion + '/eleccion/crear',
            headers=request.headers,
            json=request.json
        )

        return handle_response(peticion.json(), peticion.status_code)
    except Exception as err:
        return handle_error(err)

@app.route("/eleccion/obtener", methods=["GET"])
def eleccion_obtener():
    try:
        peticion = requests.get(
            url_eleccion + '/eleccion/obtener',
            headers=request.headers,
        )

        return handle_response(peticion.json(), peticion.status_code)
    except Exception as err:
        return handle_error(err)

@app.route("/eleccion/obtener/<eleccion>", methods=["GET"])
def eleccion_obtener_eleccion(eleccion):
    try:
        peticion = requests.get(
            url_eleccion + '/eleccion/obtener/{}'.format(eleccion),
            headers=request.headers 
        )

        return handle_response(peticion.json(), peticion.status_code)
    except Exception as err:
        return handle_error(err)

@app.route("/opcion/crear", methods=["POST"])
def opcion_crear():
    try:
        peticion = requests.post(
            url_eleccion + '/opcion/crear',
            headers=request.headers,
            json=request.json
        )

        return handle_response(peticion.json(), peticion.status_code)
    except Exception as err:
        return handle_error(err)

@app.route("/opcion/obtener/<eleccion>", methods=["GET"])
def opcion_obtener_eleccion(eleccion):
    try:
        peticion = requests.get(
            url_eleccion + '/opcion/obtener/{}'.format(eleccion),
            headers=request.headers
        )

        return handle_response(peticion.json(), peticion.status_code)
    except Exception as err:
        return handle_error(err)

@app.route("/autenticacion", methods=["GET"])
def autenticacion():
    try:
        peticion = requests.post(
            url_login + '/verify',
            headers=request.headers
        )

        return handle_response(peticion.json(), peticion.status_code)
    except Exception as err:
        return handle_error(err)
    
@app.route("/login", methods=["POST"])
def login():
    try:
        peticion = requests.post(
            url_login + '/login',
            headers=request.headers,
            json=request.json
        )

        return handle_response(peticion.json(), peticion.status_code)
    except Exception as err:
        return handle_error(err)

@app.route("/votacion/votar", methods=["POST"])
def votacion_votar():
    try:
        peticion = requests.post(
            url_votacion + '/Votacion/Votar',
            headers=request.headers,
            json=request.json
        )

        return handle_response(peticion.json(), peticion.status_code)
    except Exception as err:
        return handle_error(err)

@app.route("/votacion/cargarVotos", methods=["POST"])
def votacion_cargar_votos():
    try:
        peticion = requests.post(
            url_votacion + '/Votacion/CargarVotos',
            headers=request.headers,
            json=request.json
        )

        return handle_response(peticion.json(), peticion.status_code)
    except Exception as err:
        return handle_error(err)

@app.route("/resultados/elecciones", methods=["GET"])
def resultados_elecciones():
    try:
        peticion = requests.get(
            url_reportes + '/resultados/elecciones',
            headers=request.headers
        )

        return handle_response(peticion.json(), peticion.status_code)
    except Exception as err:
        return handle_error(err)

@app.route("/resultados/dashboard/<id_eleccion>", methods=["GET"])
def resultados_dashboard(id_eleccion):
    try:
        peticion = requests.get(
            url_reportes + '/resultados/dashboard/{}'.format(id_eleccion),
            headers=request.headers
        )

        return handle_response(peticion.json(), peticion.status_code)
    except Exception as err:
        return handle_error(err)

@app.route("/resultados/mapa", methods=["GET"])
def resultados_mapa():
    try:
        peticion = requests.get(
            url=url_reportes + "/resultados/mapa",
            headers=request.headers
        )

        return handle_response(peticion.json(), peticion.status_code)
    except Exception as err:
        return handle_error(err)

@app.route("/resultados/mapa/<id_departamento>", methods=["GET"])
def resultados_mapa_id_departamento(id_departamento):
    try:
        peticion = requests.get(
            url_reportes + '/resultados/mapa/{}'.format(id_departamento),
            headers=request.headers
        )

        return handle_response(peticion.json(), peticion.status_code)
    except Exception as err:
        return handle_error(err)

@app.route("/registro", methods=["POST"])
def registro():
    try:
        peticion = requests.post(
            url_registro + '/registro',
            headers=request.headers
        )

        return handle_response(peticion.json(), peticion.status_code)
    except Exception as err:
        return handle_error(err)

@app.route("/paises", methods=["GET"])
def paises():
    try:
        peticion = requests.get(
            url_registro + '/paises',
            headers=request.headers
        )

        return handle_response(peticion.json(), peticion.status_code)
    except Exception as err:
        return handle_error(err)

@app.route("/municipios", methods=["GET"])
def municipios():
    try:
        peticion = requests.get(
            url_registro + '/municipios',
            headers=request.headers
        )

        return handle_response(peticion.json(), peticion.status_code)
    except Exception as err:
        return handle_error(err)

@app.route("/departamentos", methods=["GET"])
def departamentos():
    try:
        peticion = requests.get(
            url_registro + '/departamentos',
            headers=request.headers
        )

        return handle_response(peticion.json(), peticion.status_code)
    except Exception as err:
        return handle_error(err)

def handle_response(body, code):
    return Response(
        json.dumps(body),
        status=code
    )

def handle_error(err):
    error_json['mensaje'] = error_json['mensaje'].format(err)
    return Response(
        json.dumps(error_json),
        status=500
    )   