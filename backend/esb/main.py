import os
from app import app

port = os.environ['port'] if 'port' in os.environ else "5006"
if __name__ == "__main__":
    app.run(port=port, host="0.0.0.0")