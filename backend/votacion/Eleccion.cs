﻿using MySql.Data.MySqlClient;

namespace votacion
{
    public class Eleccion
    {
        public string eleccion { get; set; }
        public string titulo { get; set; }
        public string descripcion { get; set; }
        public string fecha_inicio { get; set; }
        public string fecha_fin { get; set; }
        public string estado { get; set; }
        public bool activa { get; set; }

        public Eleccion() { }
        public Eleccion(string eleccion)
        {
            var dbCon = DBConnection.Instance();
            dbCon.Server = "35.224.18.92";
            dbCon.DatabaseName = "yovoto";
            dbCon.UserName = "root";
            dbCon.Password = "52*?qD;f>5-+aP/S";
            if (dbCon.IsConnect())
            {
                string query = "select * from Elecciones where eleccion = " + eleccion + " and CURDATE() between fecha_inicio and fecha_fin;";
                var cmd = new MySqlCommand(query, dbCon.Connection);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    this.eleccion = reader.GetString(0);
                    this.titulo = reader.GetString(1);
                    this.descripcion = reader.GetString(2);
                    this.fecha_inicio = reader.GetString(3);
                    this.fecha_fin = reader.GetString(4);
                    this.estado = reader.GetString(5);
                }
                dbCon.Close();
            }

            if (this.eleccion != null)
            {
                this.activa = true;
            }
            else
            {
                this.activa = false;
            }
        }

        public bool EsOpcionValida(string opcion)
        {
            string result = "";

            var dbCon = DBConnection.Instance();
            dbCon.Server = "35.224.18.92";
            dbCon.DatabaseName = "yovoto";
            dbCon.UserName = "root";
            dbCon.Password = "52*?qD;f>5-+aP/S";
            if (dbCon.IsConnect())
            {
                string query = "select * from Opciones where eleccion = " + this.eleccion + " and opcion = " + opcion + ";";
                var cmd = new MySqlCommand(query, dbCon.Connection);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result += reader.GetString(0);
                }
                dbCon.Close();
            }


            if (result.Length <= 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool YaVoto(string usuario)
        {
            string result = "";

            var dbCon = DBConnection.Instance();
            dbCon.Server = "35.224.18.92";
            dbCon.DatabaseName = "yovoto";
            dbCon.UserName = "root";
            dbCon.Password = "52*?qD;f>5-+aP/S";
            if (dbCon.IsConnect())
            {
                string query = "select * from Votos_electronicos inner join Opciones on Votos_electronicos.opcion = Opciones.opcion ";
                query += "where Opciones.eleccion = " + this.eleccion + " and Votos_electronicos.ciudadano = " + usuario + "; ";

                var cmd = new MySqlCommand(query, dbCon.Connection);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result += reader.GetString(0);
                }
                dbCon.Close();
            }

            if (result.Length <= 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        public static void CargarVotoFisico(string cantidad, string opcion, string pais, string municipio)
        {
            var dbCon = DBConnection.Instance();
            dbCon.Server = "35.224.18.92";
            dbCon.DatabaseName = "yovoto";
            dbCon.UserName = "root";
            dbCon.Password = "52*?qD;f>5-+aP/S";
            if (dbCon.IsConnect())
            {

                MySqlCommand cmd = dbCon.Connection.CreateCommand();
                cmd.CommandText = "INSERT INTO Votos_fisicos (cantidad, opcion, pais, municipio) VALUES (@cantidad, @opcion, @pais, @municipio)";
                cmd.Parameters.AddWithValue("@cantidad", cantidad);
                cmd.Parameters.AddWithValue("@opcion", opcion);
                cmd.Parameters.AddWithValue("@pais", pais);
                cmd.Parameters.AddWithValue("@municipio", municipio);
                cmd.ExecuteReader();
                dbCon.Close();
            }
        }
    }
}
