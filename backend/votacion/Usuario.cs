﻿using MySql.Data.MySqlClient;

namespace votacion
{
    public class Usuario
    {
        public string ciudadano { get; set; }
        public string cui { get; set; }
        public string token { get; set; }
        public string extranjero { get; set; }
        public string pais { get; set; }
        public string municipio { get; set; }
        public bool activo { get; set; }
        public Usuario() { }

        public Usuario(string ciudadano)
        {
            var dbCon = DBConnection.Instance();
            dbCon.Server = "35.224.18.92";
            dbCon.DatabaseName = "yovoto";
            dbCon.UserName = "root";
            dbCon.Password = "52*?qD;f>5-+aP/S";
            if (dbCon.IsConnect())
            {
                string query = "select ciudadano, cui, token, extranjero, pais, municipio from Ciudadanos where ciudadano = " + ciudadano + ";";
                var cmd = new MySqlCommand(query, dbCon.Connection);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    this.ciudadano = reader.GetString(0);
                    this.cui = reader.GetString(1);
                    this.token = reader.GetString(2);
                    this.extranjero = reader.GetString(3);
                    this.pais = reader.GetString(4);
                    this.municipio = reader.GetString(5);
                }
                dbCon.Close();
            }

            if (this.ciudadano != null)
            {
                this.activo = true;
            }
            else
            {
                this.activo = false;
            }
        }


        public bool Autenticar()
        {
            string result = "";

            var dbCon = DBConnection.Instance();
            dbCon.Server = "35.224.18.92";
            dbCon.DatabaseName = "yovoto";
            dbCon.UserName = "root";
            dbCon.Password = "52*?qD;f>5-+aP/S";
            if (dbCon.IsConnect())
            {
                string query = "select ciudadano from Ciudadanos where ciudadano = " + this.ciudadano + " and cui = " + this.cui + ";";
                var cmd = new MySqlCommand(query, dbCon.Connection);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result += reader.GetString(0);

                }
                dbCon.Close();
            }

            if (result.Length <= 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void Votar(string opcion)
        {
            var dbCon = DBConnection.Instance();
            dbCon.Server = "35.224.18.92";
            dbCon.DatabaseName = "yovoto";
            dbCon.UserName = "root";
            dbCon.Password = "52*?qD;f>5-+aP/S";
            if (dbCon.IsConnect())
            {

                MySqlCommand cmd = dbCon.Connection.CreateCommand();
                cmd.CommandText = "INSERT INTO Votos_electronicos (blockchain, opcion, ciudadano, pais, municipio) VALUES (@blockchain, @opcion, @ciudadano, @pais, @municipio)";
                cmd.Parameters.AddWithValue("@blockchain", "JXX36XTN6EI50PNY8RX");
                cmd.Parameters.AddWithValue("@opcion", opcion);
                cmd.Parameters.AddWithValue("@ciudadano", this.ciudadano);
                cmd.Parameters.AddWithValue("@pais", this.pais);
                cmd.Parameters.AddWithValue("@municipio", this.municipio);
                cmd.ExecuteReader();
                dbCon.Close();
            }
        }

    }
}
