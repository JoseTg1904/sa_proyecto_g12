﻿using JWT;
using JWT.Algorithms;
using JWT.Serializers;
using Newtonsoft.Json;

namespace votacion
{
    public class Autenticador
    {

        public static Payload Autenticar(string token)
        {
            string secret = "softwareavanzado";
            try
            {
                IJsonSerializer serializer = new JsonNetSerializer();
                IDateTimeProvider provider = new UtcDateTimeProvider();
                IJwtValidator validator = new JwtValidator(serializer, provider);
                IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
                IJwtAlgorithm algorithm = new HMACSHA256Algorithm(); // symmetric
                IJwtDecoder decoder = new JwtDecoder(serializer, validator, urlEncoder, algorithm);

                var json = decoder.Decode(token, secret, verify: true);
                Payload payload = JsonConvert.DeserializeObject<Payload>(json);
                return payload;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Token has expired");
            }
            return null;
        }

        public static string CrearTokenRestaurante()
        {
            var payload = new Dictionary<string, object>
            {
                { "error", false },
                { "rol", "restaurante" },
                { "user", "user" }
            };
            string secret = "softwareavanzado";

            IJwtAlgorithm algorithm = new HMACSHA256Algorithm(); // symmetric
            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);

            var token = encoder.Encode(payload, secret);
            return token;
        }

        public static string CrearTokenRepartidor()
        {
            var payload = new Dictionary<string, object>
            {
                { "error", false },
                { "rol", "repartidor" },
                { "user", "user" }
            };
            string secret = "softwareavanzado";

            IJwtAlgorithm algorithm = new HMACSHA256Algorithm(); // symmetric
            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);

            var token = encoder.Encode(payload, secret);
            return token;
        }
    }
}
