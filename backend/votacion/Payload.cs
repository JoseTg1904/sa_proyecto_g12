﻿namespace votacion
{
    public class Payload
    {
        public bool error { get; set; }
        public string rol { get; set; }
        public string user { get; set; }
    }
}
