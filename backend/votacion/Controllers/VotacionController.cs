﻿using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Dynamic;
using System.Net.Http.Headers;
using System.Text.Json;

namespace votacion.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VotacionController : Controller
    {
        string codUsuario = "1";
        string codOpcion = "11";
        string codEleccion = "4";

        // Realizar un voto
        [HttpPost("Votar")]
        public JsonResult Votar([FromBody] object body)
        {
            // Validar que en el encabezado venga autenticado el usuario
            //string json = JsonConvert.SerializeObject(body);
            dynamic dynData = JsonConvert.DeserializeObject<ExpandoObject>(body.ToString(), new ExpandoObjectConverter());
            string mensaje = ""; 

            // capturar datos de peticion
            foreach (KeyValuePair<string, object> transItem in dynData)
            {
                if (transItem.Key == "codUsuario")
                {
                    codUsuario = Convert.ToString(transItem.Value);

                }
                else if (transItem.Key == "codOpcion")
                {
                    codOpcion = Convert.ToString(transItem.Value);

                }
                else if (transItem.Key == "codEleccion")
                {
                    codEleccion = Convert.ToString(transItem.Value);
                }
            }

            // Construir el usuario en base a los datos recibidos en el payload (?
            Usuario usuario = new Usuario(codUsuario);

            // Autenticar el usuario
            if (!usuario.activo)
            {
                Response.StatusCode = 401;
                mensaje = "Usuario Inválido";
                return Json(mensaje);
            }

            // Obtener datos de eleccion y validar que este activa
            Eleccion eleccion = new Eleccion(codEleccion);
            if (!eleccion.activa)
            {
                Response.StatusCode = 401;
                mensaje = "Elección Inválida";
                return Json(mensaje);
            }

            // Validar opcion escogida
            if (!eleccion.EsOpcionValida(codOpcion))
            {
                Response.StatusCode = 401;

                mensaje = "Opción Inválida";
                return Json(mensaje);
            }

            // Solo puede votar una vez
            if (eleccion.YaVoto(usuario.ciudadano))
            {
                Response.StatusCode = 401;
                mensaje = "Usuario ya votó";
                return Json(mensaje);
            }

            // realizar voto
            usuario.Votar(codOpcion);


            Response.StatusCode = 200;
            var settings = new JsonSerializerSettings { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver(), Formatting = Formatting.Indented };

            mensaje = "Correcto";
            return Json(mensaje);
        }


        // Consultar Votos Existentes
        [HttpPost("ConsultarVotos")]
        public string ConsultarVotos()
        {
            //var header = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
            //var credentials = header.Parameter;
            //string token = credentials.ToString();
            //Payload payload = Autenticador.Autenticar(token);

            //if (payload == null)
            //{
            //    Response.Clear();
            //    Response.StatusCode = 401; // or whatever code is appropriate
            //    return -1;
            //}

            //if (!payload.rol.ToLower().Equals("restaurante"))
            //{
            //    Response.Clear();
            //    Response.StatusCode = 401; // or whatever code is appropriate
            //    return -2;
            //}

            var dbCon = DBConnection.Instance();
            dbCon.Server = "35.224.18.92";
            dbCon.DatabaseName = "yovoto";
            dbCon.UserName = "root";
            dbCon.Password = "52*?qD;f>5-+aP/S";
            string result = "";
            if (dbCon.IsConnect())
            {
                //suppose col0 and col1 are defined as VARCHAR in the DB
                string query = "SELECT * FROM Votos_electronicos";
                var cmd = new MySqlCommand(query, dbCon.Connection);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string someStringFromColumnZero = reader.GetString(0);
                    string someStringFromColumnOne = reader.GetString(1);
                    Console.WriteLine(someStringFromColumnZero + "," + someStringFromColumnOne);
                    result += someStringFromColumnZero + "," + someStringFromColumnOne;
                }
                dbCon.Close();
            }

            Response.StatusCode = 201;
            //var bytes = System.Text.Encoding.UTF8.GetBytes(result);
            //Response.Body.WriteAsync(bytes, 0, bytes.Length);
            return result;

        }


        // Realizar un voto
        [HttpPost("CargarVotos")]
        public JsonResult CargarVotos([FromBody] object[] body)
        {
            // Validar que en el encabezado venga autenticado el usuario
            //string json = JsonConvert.SerializeObject(body);
            foreach (object o in body)
            {
                dynamic dynData = JsonConvert.DeserializeObject<ExpandoObject>(o.ToString(), new ExpandoObjectConverter());

                string cantidad = "";
                string opcion = "";
                string pais = "";
                string municipio = "";


                // capturar datos de peticion
                foreach (KeyValuePair<string, object> transItem in dynData)
                {
                    if (transItem.Key == "cantidad")
                    {
                        cantidad = Convert.ToString(transItem.Value);

                    }
                    else if (transItem.Key == "opcion")
                    {
                        opcion = Convert.ToString(transItem.Value);

                    }
                    else if (transItem.Key == "pais")
                    {
                        pais = Convert.ToString(transItem.Value);
                    }
                    else if (transItem.Key == "municipio")
                    {
                        municipio = Convert.ToString(transItem.Value);
                    }
                }
                Eleccion.CargarVotoFisico(cantidad, opcion, pais, municipio);
            }


            Response.StatusCode = 200;
            string mensaje = "Correcto";
            return Json(mensaje);

        }


    }
}
