import json
import os
import jwt
import mysql.connector as bd

from flask import Flask, Response, request
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

"""
    Conjunto de variables globales
"""
config = {
    'user': 'root',
    'password': "52*?qD;f>5-+aP/S",
    'host': "35.224.18.92",
    'database': 'yovoto',
}
 
error_message = "Algo salio mal: {}"

"""
    Metodos y funciones
"""
@app.route("/renap/crear", methods=["POST"])
def crear():
    ciudadanos = request.json

    estado, mensaje, codigo = validar_token(request.headers)
    if not estado:
        return Response(json.dumps({"estado": mensaje}), status=codigo)

    for ciudadano in ciudadanos:
        try:
            cnx = bd.connect(**config)
            cursor = cnx.cursor()
            
            query = ("INSERT INTO Renap" 
                    " (cui, nombres, apellidos, fecha_nacimiento, puede_votar)" 
                    " VALUES (%s, %s, %s, %s, {})").format(ciudadano["puede_votar"])  
            
            cursor.execute(query, 
                (
                    ciudadano["cui"], 
                    ciudadano["nombres"],
                    ciudadano["apellidos"],
                    ciudadano["fecha_nacimiento"]
                )
            )
            
            cursor.close()
            cnx.commit()

            cnx.close()
        except Exception as err:
            print(error_message.format(err))

    return Response(json.dumps({"estado": "ciudadanos cargados al RENAP"}), status=201)

@app.route("/renap/validar/<cui>", methods=["GET"])
def validar(cui):
    contador = 0

    estado, mensaje, codigo = validar_token(request.headers)
    if not estado:
        return Response(json.dumps({"estado": mensaje}), status=codigo)

    try:
        cnx = bd.connect(**config)
        cursor = cnx.cursor(dictionary=True)
        query = "SELECT * FROM Renap WHERE cui like '{}' limit 1".format(cui)  
        cursor.execute(query)
        for _ in cursor: 
            contador += 1
        
        cursor.close()
        cnx.commit()
        cnx.close()
    except Exception as err: print(error_message.format(err))

    (codigo, mensaje) = (200, "El usuario existe") if contador == 1 else (404, "El usuario no existe") 

    return Response(json.dumps({"response": mensaje}), status=codigo)

@app.route("/renap/informacion/<cui>", methods=["GET"])
def informacion(cui):
    ciudadano = {}

    estado, mensaje, codigo = validar_token(request.headers)
    if not estado:
        return Response(json.dumps({"estado": mensaje}), status=codigo)

    try:
        cnx = bd.connect(**config)
        cursor = cnx.cursor(dictionary=True)

        query = "SELECT * FROM Renap WHERE cui like '{}' limit 1".format(cui)  
        cursor.execute(query)
        for row in cursor: 
            ciudadano = row
            break
        
        cursor.close()
        cnx.commit()
        cnx.close()
    except Exception as err: print(error_message.format(err)) 

    codigo = 200 if ciudadano != {} else 404 

    return Response(json.dumps(ciudadano), status=codigo)

def validar_token(headers):
    if ('Authorization' not in headers) or ('authorization' not in headers):
        return False, "token no encontrado", 400
   
    try:
        bearer = headers['Authorization']
        token = bearer.split()[1]
        
        jwt.decode(token, "savacacionessisale", algorithms="HS256")
        return True, "", 200
    except Exception:
        return False, "Token invalido", 401