import unittest

from app import app

headers = {
    'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IklEIGRlbCB1c3VhcmlvIiwiY3VpIjoiQ1VJIGRlbCB1c3VhcmlvIiwicGluIjoiUElOIHBhcmEgaW5pY2lhciBzZXNpw7NuIn0.-i96fD_jkHk3sn7e4UB-OgmsI9_30usOVQYkx0BYaQY'
}

headersMalos = {
    'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIn0.Rq8IxqeX7eA6GgYxlcHdPFVRNFFZc5rEI3MQTZZbK3I'
}

class UsersTest(unittest.TestCase):
    def setUp(self):
        self.ctx = app.app_context()
        self.ctx.push()
        self.client = app.test_client()

    def tearDown(self):
        self.ctx.pop()
    
    def test_crear_renap(self):
        print("creando renap")
        body = [
            {
                'puede_votar': '0', 
                'cui': '2897107250101', 
                'nombres': 'Jose Carlos I',
                'apellidos': 'Alonzo Colocho',
                'fecha_nacimiento': '19/04/1997'
            },
            {
                'puede_votar': '1',
                'cui': '2897107260101', 
                'nombres': 'Jose Carlos I',
                'apellidos': 'Alonzo Colocho',
                'fecha_nacimiento': '19/04/1997'
            }
        ]
        response = self.client.post("/renap/crear", json=body, headers=headers)
        print(response.data)
        self.assertEqual(response.status_code, 201)

    def test_crear_renap_sin_token(self):
        print("creando renap")
        body = [
            {
                'puede_votar': '0', 
                'cui': '2897107250101', 
                'nombres': 'Jose Carlos I',
                'apellidos': 'Alonzo Colocho',
                'fecha_nacimiento': '19/04/1997'
            },
            {
                'puede_votar': '1',
                'cui': '2897107260101', 
                'nombres': 'Jose Carlos I',
                'apellidos': 'Alonzo Colocho',
                'fecha_nacimiento': '19/04/1997'
            }
        ]
        response = self.client.post("/renap/crear", json=body)
        self.assertEqual(response.status_code, 400)

    def test_crear_renap_con_error(self):
        print("creando renap")
        body = [
            {
                'cui': '2897107250101', 
                'nombres': 'Jose Carlos I',
                'apellidos': 'Alonzo Colocho',
                'fecha_nacimiento': '19/04/1997'
            },
            {
                'puede_votar': '1',
                'cui': '2897107250101', 
                'nombres': 'Jose Carlos I',
                'apellidos': 'Alonzo Colocho',
                'fecha_nacimiento': '19/04/1997'
            }
        ]
        response = self.client.post("/renap/crear", json=body, headers=headers)
        self.assertEqual(response.status_code, 201)

    def test_crear_renap_con_mal_secret(self):
        print("creando renap")
        body = [
            {
                'cui': '2897107250101', 
                'nombres': 'Jose Carlos I',
                'apellidos': 'Alonzo Colocho',
                'fecha_nacimiento': '19/04/1997'
            }
        ]
        response = self.client.post("/renap/crear", json=body, headers=headersMalos)
        self.assertEqual(response.status_code, 401)

    def test_obtener_informacion(self):
        print("obteniendo informacion")
        response = self.client.get('/renap/informacion/2897107250101', headers=headers)
        self.assertEqual(response.status_code, 200)

    def test_obtener_informacion_mal_secret(self):
        print("obteniendo informacion")
        response = self.client.get('/renap/informacion/2897107250101', headers=headersMalos)
        self.assertEqual(response.status_code, 401)

    def test_obtener_validar(self):
        print("obteniendo informacion")
        response = self.client.get('/renap/validar/2897107250101', headers=headers)
        self.assertEqual(response.status_code, 200)

    def test_obtener_validar_mal_secret(self):
        print("obteniendo informacion")
        response = self.client.get('/renap/validar/2897107250101', headers=headersMalos)
        self.assertEqual(response.status_code, 401)

if __name__ == '__main__':
    unittest.main()