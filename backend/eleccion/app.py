import json
import jwt
import os
import base64
import mysql.connector as bd

from flask import Flask, Response, request
from flask_cors import CORS
from datetime import datetime
from google.cloud import storage

app = Flask(__name__)
CORS(app)

"""
    Conjunto de variables globales
"""
config = {
    'user': 'root',
    'password': "52*?qD;f>5-+aP/S",
    'host': "35.224.18.92",
    'database': 'yovoto',
    'raise_on_warnings': True
}
 
if 'GOOGLE_APPLICATION_CREDENTIALS' not in os.environ: os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = "./storage.json"
mensaje_base = "Something went wrong: {}"
formato_fecha = '%d/%m/%Y %H:%M:%S'

storageClient = storage.Client()
bucket = storageClient.get_bucket("sa-g12-bucket")

"""
    Metodos y funciones
"""
@app.route("/eleccion/crear", methods=["POST"])
def crear_eleccion():
    eleccion = request.json
    inserto = False

    estado, mensaje, codigo = validar_token(request.headers)
    if not estado:
        return Response(json.dumps({"estado": mensaje}), status=codigo)
    
    try:
        cnx = bd.connect(**config)
        cursor = cnx.cursor()
        
        query = ("INSERT INTO Elecciones" 
                " (titulo, descripcion, fecha_inicio, fecha_fin)" 
                " VALUES (%s, %s, STR_TO_DATE(%s,'%d/%m/%Y %H:%i'), STR_TO_DATE(%s,'%d/%m/%Y %H:%i'))")  
        
        cursor.execute(query, 
            (
                eleccion["titulo"], 
                eleccion["descripcion"],
                eleccion["fecha_inicio"],
                eleccion["fecha_fin"]
            )
        )
        
        cursor.close()
        cnx.commit()

        cnx.close()
        inserto = True
    except Exception as err: print(mensaje_base.format(err))
    
    (mensaje, codigo) = ("creado", 201) if inserto else ("no creado", 400) 

    return Response(json.dumps({"estado": mensaje}), status=codigo)

@app.route("/eleccion/obtener", methods=["GET"])
def obtener_todas():
    elecciones = []
    encontrado = False

    estado, mensaje, codigo = validar_token(request.headers)
    if not estado:
        return Response(json.dumps({"estado": mensaje}), status=codigo)

    try:
        cnx = bd.connect(**config)
        cursor = cnx.cursor(dictionary=True)

        query = "SELECT * FROM Elecciones"  
        cursor.execute(query)

        for row in cursor: 
            row['fecha_inicio'] = row['fecha_inicio'].strftime(formato_fecha)
            row['fecha_fin'] = row['fecha_fin'].strftime(formato_fecha)
            row['estado'] = False if row['estado'] == 0 else True
            elecciones.append(row)

        cursor.close()
        cnx.commit()

        cnx.close()
        encontrado = True
    except Exception as err: print(mensaje_base.format(err))
    
    codigo = 200 if encontrado else 400 

    return Response(json.dumps(elecciones), status=codigo)

@app.route("/eleccion/obtener/<int:eleccion>", methods=["GET"])
def obtener_unica(eleccion):
    elecciones = {}
    encontrado = False

    estado, mensaje, codigo = validar_token(request.headers)
    if not estado:
        return Response(json.dumps({"estado": mensaje}), status=codigo)

    try:
        cnx = bd.connect(**config)
        cursor = cnx.cursor(dictionary=True)

        query = "SELECT * FROM Elecciones WHERE eleccion = {}".format(eleccion)  
        cursor.execute(query)

        for row in cursor: 
            row['fecha_inicio'] = row['fecha_inicio'].strftime(formato_fecha)
            row['fecha_fin'] = row['fecha_fin'].strftime(formato_fecha)
            row['estado'] = False if row['estado'] == 0 else True
            elecciones = row

        cursor.close()
        cnx.commit()

        cnx.close()
        encontrado = True
    except Exception as err: print(mensaje_base.format(err))
    
    codigo = 200 if encontrado else 404 

    return Response(json.dumps(elecciones), status=codigo)

@app.route("/opcion/crear", methods=["POST"])
def crear_opcion():
    opcion = request.json
    inserto = False

    estado, mensaje, codigo = validar_token(request.headers)
    if not estado:
        return Response(json.dumps({"estado": mensaje}), status=codigo)

    try:
        imagen = base64.b64decode(opcion["imagen"])
        url = "{}-{}.{}".format(opcion['titulo'], datetime.now().strftime("%Y-%m-%d_%H-%M-%S"), opcion['extension'])
        subida = cargar_archivo(imagen, url)

        if subida:
            cnx = bd.connect(**config)
            cursor = cnx.cursor()

            query = ("INSERT INTO Opciones" 
                    " (titulo, imagen, metadatos, eleccion)" 
                    " VALUES (%s, %s, %s, {})").format(opcion["eleccion"])  
            
            cursor.execute(query, 
                (
                    opcion["titulo"], 
                    url,
                    opcion["metadatos"]
                )
            )
            
            cursor.close()
            cnx.commit()
            cnx.close()
            inserto = True
    except Exception as err: print(mensaje_base.format(err))
    
    (mensaje, codigo) = ("creado", 201) if inserto else ("no creado", 400) 

    return Response(json.dumps({"estado": mensaje}), status=codigo)

@app.route("/opcion/obtener/<int:eleccion>", methods=["GET"])
def obtener_por_eleccion(eleccion):
    opciones = []
    encontro = False

    estado, mensaje, codigo = validar_token(request.headers)
    if not estado:
        return Response(json.dumps({"estado": mensaje}), status=codigo)

    try:
        cnx = bd.connect(**config)
        cursor = cnx.cursor(dictionary=True)
        
        query = ("SELECT opcion, imagen, titulo, eleccion FROM Opciones WHERE eleccion = {}").format(eleccion)
        cursor.execute(query)
        
        for row in cursor: 
            row['imagen'] = "https://storage.googleapis.com/sa-g12-bucket/{}".format(row['imagen'])
            opciones.append(row)

        cursor.close()
        cnx.commit()

        cnx.close()
        encontro = True
    except Exception as err: print(mensaje_base.format(err))
    
    codigo = 200 if encontro else 404 

    return Response(json.dumps(opciones), status=codigo)

def cargar_archivo(stream, nombre):
    try:
        blob = bucket.blob(nombre)
        blob.upload_from_string(stream)
        return True
    except Exception as err:
        print(err)
        return False

def validar_token(headers):
    if ('Authorization' not in headers) or ('authorization' not in headers):
        return False, "token no encontrado", 400
   
    try:
        bearer = headers['Authorization']
        token = bearer.split()[1]

        jwt.decode(token, "savacacionessisale", algorithms="HS256")
        return True, "", 200
    except Exception:
        return False, "Token invalido", 401