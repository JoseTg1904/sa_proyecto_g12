const mysql = require('mysql');
const express = require('express');
const bodyParser = require('body-parser');
const {Storage} = require('@google-cloud/storage');
const mimeTypes = require('mimetypes');
// create express app
const app = express();
// Setup server port
const port = process.env.PORT || 5005;
const storage = new Storage({keyFilename: './storage.json'});
const bucket = storage.bucket('sa-g12-bucket');
// parse requests of content-type - application/x-www-form-urlencoded
const cors = require("cors");
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }))
// parse requests of content-type - application/json
app.use(bodyParser.json())
//MySQL details
var mysqlConnection = mysql.createConnection({
  host: '35.224.18.92',
  user: 'root',
  password: '52*?qD;f>5-+aP/S',
  database: 'yovoto',
  multipleStatements: true
  });
mysqlConnection.connect((err)=> {
  if(!err)
  console.log('Connection Established Successfully');
  else
  console.log('Connection Failed!'+ JSON.stringify(err,undefined,2));
  });

// define a root route
app.post('/registro', (req, res) => {
  let datos = req.body;
  var image = datos.fotografia_personal,
      mimeType = image.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/)[1],
      fileName1 = datos.cui + '-personal.' + mimeTypes.detectExtension(mimeType),
      base64EncodedImageString = image.replace(/^data:image\/\w+;base64,/, ''),
      imageBuffer = Buffer.from(base64EncodedImageString, 'base64');
  // Upload the image to the bucket
  var file = bucket.file(fileName1);

  file.save(imageBuffer, {
      metadata: { contentType: mimeType, cacheControl: 'public, max-age=31536000', }
  }, function(error) {
      if (error) {
        console.log(error);
        res.send(error);
      }
      const publicUrlPersonal = `https://storage.googleapis.com/${bucket.name}/${fileName1}`;
      var image = datos.fotografia_dpi,
      mimeType = image.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/)[1],
      fileName = datos.cui + '-dpi.' + mimeTypes.detectExtension(mimeType),
      base64EncodedImageString = image.replace(/^data:image\/\w+;base64,/, ''),
      imageBuffer = Buffer.from(base64EncodedImageString, 'base64');
      // Upload the image to the bucket
      var file = bucket.file(fileName);

      file.save(imageBuffer, {
          metadata: { contentType: mimeType, cacheControl: 'public, max-age=31536000', }
      }, function(error) {
          if (error) {
            console.log(error);
            res.send(error);
          }
          const publicUrlDPI = `https://storage.googleapis.com/${bucket.name}/${fileName}`;
          const d = new Date();
          const fecha = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
          let sql = 'INSERT INTO Ciudadanos (cui, token, extranjero, dispositivo_movil, fotografia_personal, fotografia_dpi, fecha_creacion, pin, ip, ubicacion, telefono, correo, pais, municipio) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?);';
          mysqlConnection.query(sql,[datos.cui,datos.token,datos.extranjero,datos.dispositivo_movil,publicUrlPersonal,publicUrlDPI,fecha,datos.pin,datos.ip,datos.ubicacion,datos.telefono,datos.correo,datos.pais,datos.municipio], (err, rows) => {
            if (!err){
              res.status(200)
              res.send(rows);
            }
            else{
              console.log(err);
            }
            })
      });
  });
});

app.get('/paises', (_req, res) => {
  let sql = 'SELECT * FROM Paises p WHERE pais != 0;';
  mysqlConnection.query(sql, (err, rows) => {
    if (!err){
      res.status(200)
      res.send(rows);
    } else {
      console.log(err);
    }
  })
});

app.get('/departamentos', (_req, res) => {
  let sql = 'SELECT * FROM Departamentos d WHERE departamento != 0;';
  mysqlConnection.query(sql, (err, rows) => {
    if (!err){
      res.status(200)
      res.send(rows);
    } else {
      console.log(err);
    }
  })
});

app.get('/municipios', (_req, res) => {
  let sql = 'SELECT * FROM Municipios m  WHERE municipio;';
  mysqlConnection.query(sql, (err, rows) => {
    if (!err){
      res.status(200)
      res.send(rows);
    } else {
      console.log(err);
    }
  })
});

// listen for requests
app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});

module.exports = app;