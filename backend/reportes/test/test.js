const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

const { app } = require("../index");

describe('Pruebas unitarias', () => {
    describe('Probando elecciones: ',() => {
        it('Resultados de elecciones', (done) => {
            chai.request(app)
                .get('/resultados/elecciones')
                .end((_err, res) => {
                    chai.assert.equal(res.status, 200);
                    done();
                });
        });

        it('Resultados de elecciones con id', (done) => {
            chai.request(app)
                .get('/resultados/dashboard/1')
                .end((_err, res) => {
                    chai.assert.equal(res.status, 200);
                    done();
                });
        });

        it('Resultados de elecciones con municipio', (done) => {
            chai.request(app)
                .get('/resultados/mapa')
                .end((_err, res) => {
                    chai.assert.equal(res.status, 200);
                    done();
                });
        });

        it('Resultados de elecciones por departamento con id', (done) => {
            chai.request(app)
                .get('/resultados/mapa/1')
                .end((_err, res) => {
                    chai.assert.equal(res.status, 200);
                    done();
                });
        });
    });
});