const mysql = require('mysql');
const express = require('express');
const bodyParser = require('body-parser');

const app = express();
// Setup server port
const port = process.env.PORT || 5004;
// parse requests of content-type - application/x-www-form-urlencoded
const cors = require("cors");
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

var mysqlConnection = mysql.createConnection({
  host: '35.224.18.92',
  user: 'root',
  password: '52*?qD;f>5-+aP/S',
  database: 'yovoto',
  multipleStatements: true
});

mysqlConnection.connect((err)=> {
  if(!err) console.log('Connection Established Successfully');
  else console.log('Connection Failed!'+ JSON.stringify(err, undefined, 2));
});

app.get('/resultados/elecciones', (_req, res) => {
  let sql = 'SELECT eleccion as id, titulo, CONCAT(DATE_FORMAT(fecha_inicio, "%Y-%m-%d"), ";", DATE_FORMAT(fecha_fin, "%Y-%m-%d")) as fecha FROM Elecciones;';
  mysqlConnection.query(sql, (err, rows) => {
    if (!err){
      res.status(200)
      res.send({response: rows});
    } else {
      console.log(err);
    }
  })
});

app.get('/resultados/dashboard/:id_eleccion', (req, res) => {
  let datos = req.params;
  let titulo = '';
  let fecha = '';
  let votantes_registrados = '';

  let query = 'SELECT COUNT(*) as total FROM Ciudadanos c , Elecciones e WHERE e.eleccion = ? AND c.fecha_creacion <= e.fecha_fin;'
  mysqlConnection.query(query, [datos.id_eleccion], (err, result) => {
    if (err) throw err;
    votantes_registrados = result[0].total;
  });

  query = 'SELECT titulo, CONCAT(DATE_FORMAT(fecha_inicio, "%Y-%m-%d"), ";", DATE_FORMAT(fecha_fin, "%Y-%m-%d")) as fecha FROM Elecciones e WHERE eleccion = ?;'
  mysqlConnection.query(query, [datos.id_eleccion], (err, result) => {
    if (err) throw err;
    titulo = result[0].titulo;
    fecha = result[0].fecha;
  });

  query = 'with electronicos as ('
  +' select count(*) as votos_electronicos, o.opcion from Votos_electronicos as ve, Opciones as o '
  +' where ve.opcion = o.opcion and o.eleccion = ?'
  +' group by ve.opcion),'
  +' fisicos as (Select sum(vf.cantidad) as votos_fisicos, o.opcion, o.titulo  from Votos_fisicos as vf, Opciones as o '
  +' where vf.opcion = o.opcion and o.eleccion = ?'
  +' group by vf.opcion)'
  +' select * from electronicos'
  +' inner join fisicos  on fisicos.opcion = electronicos.opcion;';
  mysqlConnection.query(query, [datos.id_eleccion, datos.id_eleccion], (err, rows) => {
    if (!err){
      let votos_porcentaje = 0;
      rows.forEach(element => {
        element.total = element.votos_electronicos + element.votos_fisicos;
        votos_porcentaje = element.total + votos_porcentaje;
      });
      rows.sort(function (a, b){
        return (b.total - a.total)
      });
      let respuesta = {
        "titulo": titulo,
        "fecha": fecha,
        "ganador": {
          "titulo": rows[0].titulo,
          "votos_totales": rows[0].total,
          "votos_porcentaje": ((rows[0].total * 100) / votos_porcentaje)
        },
        "votantes_registrados": votantes_registrados,
        "votantes_votaron": votos_porcentaje,
        "resultados_opcion": rows
      };
      res.status(200)
      res.send(respuesta);
    }else{
      console.log(err);
    }
    })
});

app.get('/resultados/mapa', (req, res) => {
  let departamentos = [];
  let elecciones = [];
  let paises = [];
  mysqlConnection.query('SELECT p.pais as id, p.nombre as pais FROM Paises p WHERE p.pais != 1 and p.pais != 0;', function (err, result) {
    if (err) throw err;
    paises = result;
  });
  mysqlConnection.query('SELECT departamento as id, nombre as titulo from Departamentos d WHERE departamento  != 0;', function (err, result) {
    if (err) throw err;
    departamentos = result;
  });
  mysqlConnection.query('SELECT eleccion as id, titulo  FROM Elecciones e;', function (err, result) {
    if (err) throw err;
    elecciones = result;
    elecciones.forEach(element => {
      departamentos.forEach(element1 => {
        mysqlConnection.query('Select sum(vf.cantidad) as votos_fisicos, o.titulo from Votos_fisicos as vf, Opciones as o, Municipios m, Departamentos d, Elecciones e'
        +' where vf.opcion = o.opcion'
        +' and m.municipio = vf.municipio'
        +' and m.departamento = d.departamento' 
        +' and e.eleccion  = o.eleccion'  
        +' and d.departamento = ?'
        +' group by vf.opcion, d.nombre, o.titulo;',
        [element1.id], function (err, result1) {
          mysqlConnection.query('select count(*) as votos_electronicos, o.titulo  from Votos_electronicos as ve, Opciones as o, Municipios m, Departamentos d , Elecciones e' 
          +' where ve.opcion = o.opcion'
          +' and m.municipio = ve.municipio'
          +' and m.departamento = d.departamento' 
          +' and e.eleccion = o.eleccion'
          +' and d.departamento = ?'
          +' group by ve.opcion, d.nombre, o.titulo;',
          [element1.id], function (err, result2) {
            const result3 = result1.concat(result2);
            element1.opciones = result3;
          });
        });
      });

      paises.forEach(element2 => {
        mysqlConnection.query('with electronicos as ('
        +' select count(*) as votos_electronicos, o.opcion from Votos_electronicos as ve, Opciones as o '
        +' where ve.opcion = o.opcion and ve.pais = ? and o.eleccion = ?'
        +' group by ve.opcion),'
        +' fisicos as (Select sum(vf.cantidad) as votos_fisicos, o.opcion, o.titulo  from Votos_fisicos as vf, Opciones as o '
        +' where vf.opcion = o.opcion and vf.pais = ? and o.eleccion = ?'
        +' group by vf.opcion)'
        +' select * from electronicos'
        +' inner join fisicos  on fisicos.opcion = electronicos.opcion;',
        [element2.id,element.id,element2.id,element.id], function (err, result4) {
          element2.resultados = result4;
        });
      });
      
      element.resultados = {departamentos, resultados_extranjero: paises};
      });
    setTimeout(() => {
      res.status(200)
      res.send({response: elecciones});
    }, 19999);
  });
});

app.get('/resultados/mapa/:id_departamento', (req, res) => {
  let datos = req.params;
  let municipios = [];
  let elecciones = [];
  let result3;
  mysqlConnection.query('SELECT m.nombre, m.municipio as id FROM Municipios m WHERE m.departamento = ?;',
  [datos.id_departamento], function (err, result) {
    if (err) throw err;
    municipios = result;
  });
  mysqlConnection.query('SELECT eleccion as id, titulo  FROM Elecciones e;', function (err, result) {
    if (err) throw err;
    elecciones = result;
    elecciones.forEach(element => {
      municipios.forEach(element1 => {
        mysqlConnection.query('Select sum(vf.cantidad) as votos_fisicos, o.titulo from Votos_fisicos as vf, Opciones as o, Municipios m, Elecciones e  '
        +' where vf.opcion = o.opcion'
        +' and m.municipio = vf.municipio' 
        +' and e.eleccion  = o.eleccion'
        +' and m.municipio = ?'
        +' group by vf.opcion, m.nombre;',
        [element1.id], function (err, result1) {
          mysqlConnection.query('select count(*) as votos_electronicos, o.titulo from Votos_electronicos as ve, Opciones as o, Municipios m, Elecciones e'
          +' where ve.opcion = o.opcion'
          +' and m.municipio = ve.municipio'
          +' and e.eleccion = o.eleccion'
          +' and m.municipio = ?'
          +' group by ve.opcion, m.nombre;',
          [element1.id], function (err, result2) {
            result3 = result1.concat(result2);
            element1.opciones = result3;
          });
        });
      });
      element.resultados = municipios;
    });
    setTimeout(() => {
      res.status(200)
      res.send({response: elecciones});
    }, 19999);
  });
});

app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});

exports.app = app;