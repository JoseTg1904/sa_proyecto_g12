CREATE DATABASE IF NOT EXISTS yovoto;

USE yovoto;

CREATE TABLE IF NOT EXISTS Elecciones (
    eleccion     INT          NOT NULL AUTO_INCREMENT,
    titulo       VARCHAR(100) NOT NULL,
    descripcion  TEXT         NOT NULL,
    fecha_inicio DATETIME     NOT NULL,
    fecha_fin    DATETIME     NOT NULL,
    estado       BOOLEAN      NOT NULL DEFAULT FALSE,
    PRIMARY KEY(eleccion)
);

CREATE TABLE IF NOT EXISTS Opciones (
    opcion    INT          NOT NULL AUTO_INCREMENT,
    titulo    VARCHAR(200) NOT NULL,
    imagen    VARCHAR(200) NOT NULL,
    metadatos TEXT         NOT NULL,
    eleccion  INT          NOT NULL,
    PRIMARY KEY(opcion),
    FOREIGN KEY(eleccion) REFERENCES Elecciones(eleccion)
);

CREATE TABLE IF NOT EXISTS Paises (
    pais   INT          NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(100) NOT NULL,
    PRIMARY KEY(pais)
);

CREATE TABLE IF NOT EXISTS Departamentos (
    departamento INT          NOT NULL AUTO_INCREMENT,
    nombre       VARCHAR(100) NOT NULL,
    pais         INT          NOT NULL,
    PRIMARY KEY(departamento),
    FOREIGN KEY(pais) REFERENCES Paises(pais)
);

CREATE TABLE IF NOT EXISTS Municipios (
    municipio    INT          NOT NULL AUTO_INCREMENT,
    nombre  VARCHAR(100) NOT NULL,
    departamento INT NOT NULL,
    PRIMARY KEY(municipio),
    FOREIGN KEY(departamento) REFERENCES Departamentos(departamento)
);

CREATE TABLE IF NOT EXISTS Votos_fisicos (
    voto_fisico INT          NOT NULL AUTO_INCREMENT,
    cantidad    INT          NOT NULL,
    opcion      INT          NOT NULL,
    pais        INT,
    municipio   INT,
    PRIMARY KEY(voto_fisico),
    FOREIGN KEY(opcion)    REFERENCES Opciones(opcion),
    FOREIGN KEY(municipio) REFERENCES Municipios(municipio),
    FOREIGN KEY(pais)      REFERENCES Paises(pais)
);

CREATE TABLE IF NOT EXISTS Ciudadanos (
    ciudadano           INT          NOT NULL AUTO_INCREMENT,
    cui                 VARCHAR(50)  NOT NULL,
    token               TEXT         NOT NULL,
    extranjero          BOOLEAN      NOT NULL,
    dispositivo_movil   TEXT         NOT NULL,
    fotografia_personal VARCHAR(200) NOT NULL,
    fotografia_dpi      VARCHAR(200) NOT NULL,
    fecha_creacion      DATETIME NOT NULL,
    pin                 TEXT NOT NULL,
    ip                  TEXT NOT NULL,
    ubicacion           TEXT NOT NULL,
    telefono            VARCHAR(20),
    correo              VARCHAR(300),
    pais                INT,
    municipio           INT,
    PRIMARY KEY(ciudadano),
    FOREIGN KEY(municipio) REFERENCES Municipios(municipio),
    FOREIGN KEY(pais)      REFERENCES Paises(pais)
);

CREATE TABLE IF NOT EXISTS Votos_electronicos (
    voto_electronico INT          NOT NULL AUTO_INCREMENT,
    blockchain       TEXT         NOT NULL,
    opcion           INT          NOT NULL,
    ciudadano        INT NOT NULL,
    pais             INT,
    municipio        INT,
    PRIMARY KEY(voto_electronico),
    FOREIGN KEY(opcion)    REFERENCES Opciones(opcion),
    FOREIGN KEY(municipio) REFERENCES Municipios(municipio),
    FOREIGN KEY(pais)      REFERENCES Paises(pais),
    FOREIGN KEY(ciudadano) REFERENCES Ciudadanos(ciudadano)
);

CREATE TABLE IF NOT EXISTS Renap (
    id               INT          NOT NULL AUTO_INCREMENT,
    cui              VARCHAR(50)  NOT NULL,
    nombres          VARCHAR(200) NOT NULL,
    apellidos        VARCHAR(200) NOT NULL,
    fecha_nacimiento VARCHAR(100) NOT NULL,
    puede_votar      BOOLEAN      NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS Cierres (
    cierre               INT NOT NULL AUTO_INCREMENT,
    eleccion             INT NOT NULL,
    opcion               INT NOT NULL,
    votos_electronicos   INT NOT NULL,
    votos_fisicos        INT NOT NULL,
    PRIMARY KEY(cierre),
    FOREIGN KEY(eleccion) REFERENCES Elecciones(eleccion),
    FOREIGN KEY(opcion)   REFERENCES Opciones(opcion)
);