const jwt = require("jsonwebtoken");

const verifyToken = (token) => {
  if (!token) {
    return "A token is required for authentication";
  }
  try {
    const decoded = jwt.verify(token, process.env.TOKEN_KEY || "savacacionessisale");
    req.user = decoded;
  } catch (err) {
    return "Invalid Token";
  }
  return next();
};

module.exports = verifyToken;