require("dotenv").config();
const express = require("express");
const bcrypt = require("bcryptjs");
var cors = require("cors");
const jwt = require("jsonwebtoken");
const auth = require("./middleware/auth");
const authData = require("./middleware/authData");
const authApi = require("./middleware/authApis");
const { json } = require("body-parser");
const mysqlConnection = require('./config/bd')

const app = express();

app.use(express.json());
app.use(cors({
    origin: '*'
}));

// Logic goes here
app.post("/login", async (req, res) => {
    const data = req.body;
    console.log(data);
    query_ =  `SELECT * FROM Ciudadanos e
               WHERE e.cui = '${data.cui}' and e.correo = '${data.email}' and e.pin = '${data.pin}'`;
    // Our login logic starts here
    try {
        // Validate user input
        if (!(data.cui)) {
            res.status(400).send("All input is required");
        }

        mysqlConnection.query(query_, function (error, results, fields) {
            if (error) throw error;
            if (results.length == 0) {
                console.log("Usuario No Registrado");
                return res.status(404).send("Usuario no autenticado");
            } else {
                console.log('logueado');
                cui = data.cui
                id = results[0].ciudadano;
                pin = results[0].pin
                console.log(cui)
                // Create token
                const token = jwt.sign(
                    { id, cui, pin },
                    process.env.TOKEN_KEY || "savacacionessisale",
                    {
                        expiresIn: "2h",
                    }
                );


                // save user token
                // results[0].token = token;

                // user
                // authData(token);
                console.log("----------------------------------------------------------------")
                console.log(({"STATUS": 200, "Message": "Welcome 🙌", results, "Date": Date()}))
                console.log("----------------------------------------------------------------")

                return res.status(200).json({"Message": "Welcome 🙌", token, "user": results[0]});


                res.status(200).json({
                    estado: true,
                    data:   results
                }); 
            }
        });
    } catch (err) {
        console.log("----------------------------------------------------------------")
        console.log(({"STATUS": 505, "Message": "Internal Server Erro", "Date": Date()}))
        console.log("----------------------------------------------------------------")
        
        // console.log(err);
        return res.status(404).send("Usuario no autenticado");
    }
    // Our register logic ends here
});



app.post("/", auth, (req, res) => {
    return res.status(200).send(req.user);
});

app.post("/verify", (req, res) => {
    console.log(req.headers)
    if (!authApi(req, res)){
        return res.status(403).json({"Status": false, "Message": "Invalid Credentials"});
    }
    return res.status(200).json({"Status": true, "Message": req.user});
});


module.exports = app;