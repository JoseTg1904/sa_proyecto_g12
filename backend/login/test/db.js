//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

const mysql = require('mysql');
//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();


chai.use(chaiHttp);
//Our parent block
describe('Apps', () => {
    /*
      * Test the /GET route
      */
    describe('/POST Verify', () => {
        it('it should not Verify', (done) => {
            chai.request(server)
                .post('/verify')
                .send('authorization', 'TCB55GFV8WT84QRI6TZ')
                .end((err, res) => {
                    res.should.have.status(403);
                    done();
                });
        });

    });

    describe('/POST llgin', () => {
        it('it should not POST a login', (done) => {
            let user = {
                pin: "7381",
                cui: "5854286441109",
                email: "nullam@outlook.ca"
            }
            chai.request(server)
                .post('/login')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });

    });
});