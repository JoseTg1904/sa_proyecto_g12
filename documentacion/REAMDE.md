# <h1 align="center">PROYECTO DE SOFTWARE AVANZADO</h1>
<h3 >Desaroolado por: <strong>GRUPO 12</strong> </h3>
<h3 >Año: 2022 </h3>

## FASE 1 - DOCUMENTACIÓN


## Análisis y solución propuesta
El sistema <strong>yoVoto</strong> realizará su primer funcionamiento en la facultad de Ingeniería de Ciencias y Sistemas de la Universidad de San Carlos de Guatemala, posteriormente en la Universidad y luego en todo el país. Permitiendo a cualquier persona interna o externa al pais pueda emitir su voto fatisfactoriamente.<br>
Por lo tanto, el presente proyecto contará con automatización en su desarrollo para evidenciar los cambios en tiempo real con total seguridad de que funcionará en cada actualización del sistema y así permitir al desarrollador trabajar de manera eficiente y enfocarse en el desarrollo de sus respectivas tareas.<br>
Para ello se estandarizarán buenas prácticas, metodologías de branching, metodologías ágiles, automatización en pruebas(unitarias, integración, funcionales y de estrés) y sobre todo se contará con CI/CD para poder garantizar un buen funcionamiento del ambiente de producción y de desarrollo.<br>
Con respecto a la metodología, se utilizará la metodología en espiral ya que el desarrollo continuo o repetido ayuda en la gestión de riesgos y un punto importante es que siempre hay espacio para atender los comentarios de los clientes.<br>
El modelo branching a utilizar será gitflow el cual ofrece entregas interactivas. También nos permitirá controlar las versiones y así trabajar cada rama por separada con la opción de tener la rama main o master en producción y la de development en el ambiente de desarrollo para pruebas internas y así probar las actualizaciones antes del pase a producción.

## Implementación
- Planificación: Se definirá entonces un cronograma, con los archivos y documentos reunidos por el equipo de trabajo. Además se desglosarán las actividades con base en el diseño del producto.
- Análisis: Se validará el prototipo pretendido, acorde a los plazos de tiempo y presupuesto entregado al cliente.
- Desarrollo: Se desarrolla y valida el prototipo (desarrollo de software, pruebas y despliegue), según el alcance y las funciones definidas en la etapa anterior.
- Evaluación: Cuando concluye una vuelta de al espiral, es momento de comenzar el ciclo de nuevo, pero no sin antes evaluar lo realizado en la iteración que termina. Esta evaluación, la realiza el jefe de desaroolo, de producción y el cliente.


## Toma de requerimientos
### Funcionales
- Login y Registro de los votantes y otros usuarios
- Validación con RENAP
- Seguridad en el login (2FA)
- Implementación de roles.
- Configuración de elección
- Configucación del sistema para votación
- Información persistente
- Accesibilidad desde cualquier dispositivo

### No funcionales
- Diferentes ambientes (Producción y Desarrollo)
- Implementación de versionamiento en ramas y el proyecto
- Implementación de una arquitectura orientada a servicios
- Uso de contenedores
- Herramienta para seguimiento de tareas
- Documentación del proyecto
- Implementar Seguridad y encriptación de datos
    - Implementar JWT
    - Implementar 2FA
- Automatización utilizando pipelines
- Comunicación entre servicios
- Realización de pruebas unitarias.


## Historias de Usuario
- **Usuario**
- Como usuario quiero registrarme en el sistema
- Como usuario quiero ingresar mi voto

- **Administrador**
- Como administrador quiero registrarme en el sistema
- Como administrador quiero crear una elección
- Como administrador quiero editar una elección
- Como administrador quiero ver el estado de una elección
- Como administrador quiero realizar reportes por elección.


## Descripción de las tecnología a utilizar
- **Frontend**
    - **Angular**: Framkework utilizado para la vista resposive.
- **Backend**
    - **Nodejs**:Se utilizará para la creación de algunos servicios de aplicaciones API Rest
    - **Puthon**:Se utilizará para la creación de algunos servicios de aplicaciones API Rest
- **Contenerizar**
    - **Docker**: Se utilizará para la gestión de contenedores.
    - **_DockerHub**: Servicio para almacenar las imagenes de docker
- **Versionamiento**
    - **Gitlab**: Servicio para la gestión del repositorio y control de las versiones del proyecto.
- **Alojamiento**Asys*2022!

    - **Google Cloud**: Utilizado para la creación de máquinas virtuales y la gestión de la base de datos
- **Seguridad**
    - **JWT**: Servicio para el uso de token en la autentificación.
    - **Google Authenticator**: Servicio para doble autentificación.
- **Visualización del rendimiento**
    - **Prometheus:**: Sistema para monitorear el proyecto. 
    - **Grafana:**: Sistema para observar gráficamente los datos y la situación del proyecto / empresa.
- **Pruebas unitarias**
    - **Mocha**: Framework para pruebas en los servicios de Nodejs.
    - **Chai**: Librería para poder integrarla con Mocha y así tener un marco más legible y cómodo al momento de realizar las pruebas.
    - **Unittest**: Librería para pruebas en Python.


## Comunicación de los microservicios
![image1](image1.png)

## Diagrama de actividades del microservicio de autenticación
![image2](image2.png)

## Diagrama de actividades de emisión de voto

![image3](image3.png)

## Diagrama de actividades del sistema de registro de ciudadanos

![image4](image4.png)

## Descripción de la seguridad de la aplicación
Se implementará autentificación 2FA el cual nos ayudará a tener una mejor seguridad en la aplicación utilizando la app que nos ofrece Google authenticator y aplicándolo al sistema de yoVoto.<br>
Para utilizar este método, se implementarán ciertas librerías para el funcionamiento del 2FA:
- **QRCODE**: Para lectura y generación del código QR y convertirla en data legible.
- **SPEAKEASY**: Se utilizará para generar y leer el algoritmo para Google Authneticator y así usarlo posteriormente.

También se implementará el uso del token JWT el cual se formará con el el id del usuario, el rol que lleva y su respectiva contraseña y asignándole una duración máxima de 1 hora.<br>
Con este estándar los datos del cliente pueden enviarse por medio de este token, por ejemplo, el id del cliente. Y así al momento de leer el token se obtendrán los datos completos del cliente por medio del ID y así redirigirlos a distintas páginas por medio de su rol.<br>
Hay varias formas de mandar y obtener un token JWT:
- Por medio del HEADER.
- Por medio de un QueryParam.
- Por medio del body.

Las 3 formas son válidas, pero por motivos de buenas prácticas se enviará por medio del HEADER.<br><br>
Para la construcción del token es necesario tener lo siguiente:
- Payload: Los campos con los que se construirá el cifrado para el token.
- Secret: Es la palabra o firma que llevará como llave para la codificación del token.



## Descripción del uso de la tecnología blockchain

No poseo conocimientos para poder documentar esta parte :(

## Documentación de las Pipelines para los servicios
Un pipeline en términos generales no es más que en esquema estructural, es decir, que cada fase que contiene tomo como entrada la salida del proceso anterior (exceptuando la fase de entrada o inicial).
- **Stages**: Fase que describe el orden de los pasos con los que se irá ejecutando.
- **Release**: En este stage cada vez que se haga un merge a la rama de desarrollo (development), haga un login a docker. Luego se procederá a construir las imaágenes que contiene el archivo _DockerFile_ y al completarse se publicará.
- **Test**: En este stage cada vez que se haga un merge a la rama de desarrollo (development), haga un login a docker. Luego dentro del contenedor ejecutándose actualmente se correrán las pruebas detallas o configuradas.
- **Deploy**: En este stage cada vez que se haga un merge a la rama de desarrollo (development), se detendrán los contenedores de las imágenes creadas en el stade Release, luego se eliminarán y como si fuera git, se hará un pull de la imagen recientemente creada a docker y luego la creará y la correrá.


## Contratos de servicios
### LOGIN
- **ID**: 001
- **Nombre**: Login
- **Descripción**: Gestiona el acceso al sistema
- **Criterios de Aceptación**: Correo y contraseña correctos.
- **Configuración**
    - **Ruta**: /login
    - **Método**: POST
    - **Formato de entrada**: JSON
    - **Formato de salida**: JSON
    - **Header (Atributo | Valor)**
        - Content-type | application/json
        - x-acces-tplen | token JWT
- **Parámetros de entrada (Atributo | Tipo | Descripción)**
    - email | cadena | correo registrado en el sistema.
    - password | cadena | contraseña registrada en el sistema.
      ~~~JSON
        {
            "email": "test@dominio.dominio",
            "password": "PASSWORD_EXAMPLE"
        }
      ~~~
- **Parámetros de salida**
    - **Respuesta exitosa**
        - **Código respuesta**: 200
        - **Parámetros de salida exitosa (Atributo | Tipo | Descripción)**
            - token | cadena | JWT único para cada usuario.
            - msg | cadena | JDescripción del problema.
                ~~~ JSON
                {
                    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZF91c3VhcmlvIjoxMjM1LCJ0aXBvIjoiQ0xJRU5URSIsImlhdCI6MTYzOTU0OTY4MH0.tqLIO2mkFGXxFMmklLWGT68jU_7ScMyOb8vrfUCZ9H4",
                    "msg": "Ejemplo de respuesta exitoso"
                }
                ~~~
    - **Respuesta fallida**
        - **Código de respuesta**: 403, 401, 500
        - **Parámetros de salida fallida (Atributo | Tipo | Descripción)**
            - token | cadena | JWT único para cada usuario.
            - msg | cadena | Descripción del problema.
                ~~~ JSON
                {
                    "token": ""
                    "msg": "Internal Server Error"
                }
                ~~~


### REGISTRO
- **ID**: 002
- **Nombre**: SignUp
- **Descripción**: Gestiona la creación de usuarios
- **Criterios de Aceptación**: Debe tener correctos los campos necesarios: Correo, nombre y apellidos y contraseña y el correo debe ser único y no estar registrado en el sistema.
- **Configuración**
    - **Ruta**: /SignUp
    - **Método**: POST
    - **Formato de entrada**: JSON
    - **Formato de salida**: JSON
    - **Header (Atributo | Valor)**
        - Content-type | application/json
        - x-acces-tplen | token JWT
- **Parámetros de entrada (Atributo | Tipo | Descripción)**
    - name | cadena | nombre del usuario.
    - email | cadena | correo del usuario.
    - password | cadena | contraseña del usuario.
      ~~~JSON
        {
            "name": "NAME_EXAMPLE",
            "email": "test@dominio.dominio",
            "password": "PASSWORD_EXAMPLE"
        }
      ~~~
- **Parámetros de salida**
    - **Respuesta exitosa**
        - **Código respuesta**: 200
        - **Parámetros de salida exitosa (Atributo | Tipo | Descripción)**
            - status | boolean | si se completo el proceso.
            - msg | cadena | JDescripción del problema.
                ~~~ JSON
                {
                    "ok": true,
                    "msg": "Ejemplo de respuesta exitoso"
                }
                ~~~
    - **Respuesta fallida**
        - **Código de respuesta**: 403, 401, 500
        - **Parámetros de salida fallida (Atributo | Tipo | Descripción)**
            - status | boolean | si se completo el proceso.
            - msg | cadena | Descripción del problema.
                ~~~ JSON
                {
                    "ok": false,
                    "msg": "Internal Server Error"
                }
                ~~~


### CREACIÓN DE ELECCIÓN (Administrador)
- **ID**: 003
- **Nombre**: Creación de la campaña de elección
- **Descripción**: Gestiona la campaña de elección (sólamente habilitado para el administrador)
- **Criterios de Aceptación**: El usuario debe ser de tipo Administrador, estar registrado en el sistema y debe tener los campos correctos para la elección.
- **Configuración**
    - **Ruta**: /admin/campaign
    - **Método**: POST
    - **Formato de entrada**: JSON
    - **Formato de salida**: JSON
    - **Header (Atributo | Valor)**
        - Content-type | application/json
        - x-acces-tplen | token JWT
- **Parámetros de entrada (Atributo | Tipo | Descripción)**
    - name | cadena | nombre del usuario.
    - description | cadena | correo del usuario.
    - start_date | fecha | Fecha de inicio de la elección para estar activa.
    - end_date | fecha | Fecha final para tener activa la elección.
      ~~~JSON
        {
            "name": "Campaign 2022",
            "description": "Description campaign 2022",
            "start_date": "01-07-2022 12:00",
            "end_date": "01-08-2022 00:00"
        }
      ~~~
- **Parámetros de salida**
    - **Respuesta exitosa**
        - **Código respuesta**: 200
        - **Parámetros de salida exitosa (Atributo | Tipo | Descripción)**
            - status | boolean | si se completo el proceso.
            - msg | cadena | JDescripción del problema.
                ~~~ JSON
                {
                    "ok": true,
                    "msg": "Ejemplo de respuesta exitoso"
                }
                ~~~
    - **Respuesta fallida**
        - **Código de respuesta**: 403, 401, 500
        - **Parámetros de salida fallida (Atributo | Tipo | Descripción)**
            - status | boolean | si se completo el proceso.
            - msg | cadena | Descripción del problema.
                ~~~ JSON
                {
                    "ok": false,
                    "msg": "Internal Server Error"
                }
                ~~~


### CREACIÓN DE OPCIONES DE VOTACIÓN POR ELECCIÓN (Administrador)
- **ID**: 004
- **Nombre**: Creación de las opciones para votación
- **Descripción**: Gestiona las opciones para la votación (sólamente habilitado para el administrador)
- **Criterios de Aceptación**: El usuario debe ser de tipo admiistrador, estar registrado en el sistema, tener una elección válida y tener los datos correctos para una votación.
- **Configuración**
    - **Ruta**: /admin/vote
    - **Método**: POST
    - **Formato de entrada**: JSON
    - **Formato de salida**: JSON
    - **Header (Atributo | Valor)**
        - Content-type | application/json
        - x-acces-tplen | token JWT
- **Parámetros de entrada (Atributo | Tipo | Descripción)**
    - name | cadena | Nombre de la opcion para votar.
    - image | cadena | imagen de la votación.
    - data | fecha | data de la votación.
      ~~~JSON
        {
            "name": "Partido 1",
            "image": "...",
            "data": {"..."}
        }
      ~~~
- **Parámetros de salida**
    - **Respuesta exitosa**
        - **Código respuesta**: 200
        - **Parámetros de salida exitosa (Atributo | Tipo | Descripción)**
            - status | boolean | si se completo el proceso.
            - msg | cadena | JDescripción del problema.
                ~~~ JSON
                {
                    "ok": true,
                    "msg": "Ejemplo de respuesta exitoso"
                }
                ~~~
    - **Respuesta fallida**
        - **Código de respuesta**: 403, 401, 500
        - **Parámetros de salida fallida (Atributo | Tipo | Descripción)**
            - status | boolean | si se completo el proceso.
            - msg | cadena | Descripción del problema.
                ~~~ JSON
                {
                    "ok": false,
                    "msg": "Internal Server Error"
                }
                ~~~


## Presentación
[Presentación](https://www.canva.com/design/DAFDOqEKQF4/ggUZlgCWewsP69tJyUFixw/view?utm_content=DAFDOqEKQF4&utm_campaign=designshare&utm_medium=link&utm_source=publishsharelink)