## Comunicación entre los servicios.

Uno de los aspectos más importantes en el desarrollo de una aplicación o un software basado en los microservicios es la comunicación que se va a establecer entre ellos, un proceso que puede resultar bastante complejo.

En un software de arquitectura monolítica y que por lo tanto se ejecuta en un único proceso, los distintos componente se comunican entre ellos mediante llamadas a nivel del lenguaje. Dichos componentes pueden estar estrechamente ligados entre sí en el caso de que se creen objetos concretos con código, o bien pueden establecerse conexiones de un modo desacoplado si se usan referencias de abstracciones utilizando una inserción de dependencias. Pero en ambos casos, los objetos se ejecutan en un mismo proceso.

[Regresar arriba](#)

## Diagrama de actividades del microservicio de autenticación. 

![Diagrama1](/documentacion/DiagramaAutenticacion.png)

[Regresar arriba](#)

## Diagrama de actividades de emisión de voto. 

![Diagrama2](/documentacion/DiagramaVoto.png)

[Regresar arriba](#)

## Diagrama de actividades del sistema de registro de ciudadanos. 

![Diagrama3](/documentacion/DiagramaRegistro.png)

[Regresar arriba](#)

## Descripción de la seguridad de la aplicación. 

La seguridad de la aplicación será por medio de JWT,

JWT (JSON Web Token) es un estándar qué está dentro del documento RFC 7519.
En el mismo se define un mecanismo para poder propagar entre dos partes, y de forma segura, la identidad de un determinado usuario, además con una serie de claims o privilegios.

Estos privilegios están codificados en objetos de tipo JSON, que se incrustan dentro de del payload o cuerpo de un mensaje que va firmado digitalmente.

Token JWT

En la práctica, se trata de una cadena de texto que tiene tres partes codificadas en Base64, cada una de ellas separadas por un punto, Como hemos dicho, un token tres partes:

- Header: encabezado dónde se indica, al menos, el algoritmo y el tipo de token, que en el caso del ejemplo anterior era el algoritmo HS256 y un token JWT.

- Payload: donde aparecen los datos de usuario y privilegios, así como toda la información que queramos añadir, todos los datos que creamos convenientes.

- Signature: una firma que nos permite verificar si el token es válido, y aquí es donde radica el quid de la cuestión, ya que si estamos tratando de hacer una comunicación segura entre partes y hemos visto que podemos coger cualquier token y ver su contenido con una herramienta sencilla.

[Regresar arriba](#)

## Descripción del uso de la tecnología blockchain. 

El Blockchain es una tecnología basada en una cadena de bloques de operaciones descentralizada y pública. Esta tecnología genera una base de datos compartida a la que tienen acceso sus participantes, los cuáles pueden rastrear cada transacción que hayan realizado. Es como un gran libro de contabilidad inmodificable y compartido que van escribiendo una gran cantidad de ordenadores de forma simultánea.

¿Cómo funciona la Tecnología Blockchain?
La cadena de bloques es un registro de todas las transacciones, almacenadas y compartidas de forma pública. Los llamados mineros se encargan de verificar esas transacciones. Tras ello, se incluyen en la cadena y se distribuyen a los nodos que forman la red.

Un bloque está constituido por un conjunto de transacciones. Cada uno forma parte de la cadena de bloques. La compañía Bit2me, especializada en el Bitcoin y su tecnología, define cada una de las partes que conforman un bloque:  

- Un código alfanumérico que enlaza con el bloque anterior.
- El “paquete” de transacciones que incluye (cuyo número viene determinado por diferentes factores).
- Otro código alfanumérico que enlazará con el siguiente bloque.

El siguiente bloque en progreso lo que intenta es averiguar con cálculos el código alfanumérico que permitía al anterior bloque enlazarse a éste.

[Regresar arriba](#)

## Documentación de las Pipelines para los servicios. 

GitLab CI/CD es una herramienta que nos permite automatizar los procesos de integración, entrega y despliegue continuo en nuestros proyectos almacenados en GitLab. A continuación vamos a ver una forma rápida y sencilla de utilizarla.

Es importante que nos acostumbremos a trabajar con practicas que nos permitan detectar y solucionar errores de forma rápida y eficiente. La integración y entrega continua son algunas de ellas, y buscan automatizar procesos tales como la ejecución de pruebas y despliegues, desligándonos a nosotros como desarrolladores de estas tareas y permitiéndonos enfocarnos puramente en el desarrollo.

Para configurar el pipeline, en este archivo definimos los stages (en mi caso, compile build y luego test) y que debe hacer en cada uno, es decir, que scripts debe ejecutar. También indicamos en cada stage que runner va a ser el que ejecutará esa tarea (en mi caso, runner-ejemplo es el tag que le puse al runner que registré anteriormente).

![Pipe](/documentacion/Pipeline.png)

[Regresar arriba](#)

## Exposición de la solución 

[Presentacion](/documentacion/ExposicionFase1.pdf)

[Regresar arriba](#)

## Contrato de servicios

# Autenticación

Se comparte únicamente servicio autenticación con un endpoint
GET: `3000/autenticacion`

**Auth**

**SECRET:** savacacionessisale

```
**Type:** Bearer Token
**Token:** JWT generado en login
```

**Status**

```
**200**: Usuario autenticado
**404**: Usuario no autenticado
```

**Response**

```json
{
	"id": "ID del usuario",
	"cui": "CUI del usuario",
	"pin": "PIN para iniciar sesión"
}
```

# RENAP

Formato del archivo de datos a cargar:

```json
[
  {
    "cui": "Código Único de Identificación del ciudadano",
    "nombres": "Nombres del ciudadano",
    "apellidos": "Apellidos del ciudadano",
    "fecha_nacimiento": "Fecha de nacimiento del ciudadano",
    "puede_votar": "0|1",
  },
  ...
]
```

**Validar usuario existe**

GET: `3001/renap/validar/:cui`

**Status**

```
**200**: Usuario existe
**404**: Usuario no existe
```

**Response**

```json
{
	"response": "El usuario existe"
}
```

**Obtener información**

GET: `3001/renap/informacion/:cui`

**Status**

```
**200**: Usuario existe
**404**: Usuario no existe
```

**Response**

```json
{
  "cui": "Código Único de Identificación del ciudadano",
  "nombres": "Nombres del ciudadano",
  "apellidos": "Apellidos del ciudadano",
  "fecha_nacimiento": "Fecha de nacimiento del ciudadano",
  "puede_votar": "0|1",
}
```

## Reportes

**Lista de elecciones**

GET: `3002/resultados/elecciones`****

Status

```json
200: Elecciones existentes
```

Response

```json
{
	"response": [
		{
		  "id": "ID de la elección",
			"titulo": "Título de la elección",
		  "fecha": "Fecha/Período en que se realizó 2022-06-01;2022-06-05"
		},
		...
	]
}
```

**Dashboard**

GET: `3002/resultados/dashboard/:id_eleccion`****

Status

```json
200: Elecciones existentes
```

Response

```json
{
  "titulo": "Título de la elección",
  "fecha": "Fecha/Período en que se realizó 2022-06-01;2022-06-05",
  "ganador": {
    "titulo": "Nombre del representante ganador",
    "votos_totales": "Número de votos del ganador",
    "votos_porcentaje": "Porcentaje de votos del ganador"
  },
  "votantes_registrados": "Número de votantes registrados al momento de la elección",
  "votantes_votaron": "Número de votantes que han emitido su voto",
  "resultados_opcion": [
    {
      "titulo": "Nombre del representante",
      "votos_electronicos": "Número de votos electrónicos hacia el representante",
      "votos_fisicos": "Número de votos físicos hacia el representante"
    },
    ...
  ],
}
```

**Resultados Gráfica mapa (Departamento o Extranjero)**

GET: `3002/resultados/mapa`

Status

```json
200: Elecciones existentes
```

Response

```json
{
  "response": [
      {
        "id": "ID de la elección",
        "titulo": "Título de la elección",
        "resultados": [
          {
            "id": "ID del departamento",
            "departamento": "Nombre del departamento",
            "opciones": [
              {
                "titulo": "Título del representante",
                "votos_electronicos": "Número de votos electrónicos hacia el representante",
                "votos_fisicos": "Número de votos físicos hacia el representante"
              },
              ...
            ]
          },
          ...
        ],
        "resultados_extranjero": [
          {
            "pais": "Nombre del país",
            "resultados": [
              {
                "titulo": "Título del representante",
                "votos_electronicos": "Número de votos electrónicos hacia el representante",
                "votos_fisicos": "Número de votos físicos hacia el representante"
              },
              ...
            ]
          },
          ...
        ],
      },
      ...
    ]
}
```

**Resultados Gráfica mapa (Municipio)**

GET: `3002/resultados/mapa/:id_departamento`

Status

```json
200: Resultados existentes
```

Response

```json
{
  "response": [
      {
        "id": "ID de la elección",
        "titulo": "Título de la elección",
        "resultados": [
          {
            "municipio": "Nombre del municipio",
            "opciones": [
              {
                "titulo": "Título del representante",
                "votos_electronicos": "Número de votos electrónicos hacia el representante",
                "votos_fisicos": "Número de votos físicos hacia el representante"
              },
              ...
            ]
          },
          ...
        ],
      },
      ...
    ]
}
```

# Modelo Entidad-Relación

![Modelo](/documentacion/er_yoVoto.png)

[Regresar arriba](#)